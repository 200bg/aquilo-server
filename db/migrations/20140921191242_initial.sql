
-- +goose Up
CREATE TABLE IF NOT EXISTS aquilo_user (
  "id"   SERIAL PRIMARY KEY,
  "email" VARCHAR(64) NOT NULL,
  "name" VARCHAR(256) NOT NULL,
  "user_token" VARCHAR(256) NOT NULL,
  "hashed_password" VARCHAR(1024) NOT NULL,
  "salt" VARCHAR(1024) NOT NULL,
  "hash_count" INTEGER DEFAULT 1500,
  "permissions_level" SMALLINT DEFAULT 0,
  "is_banned" BOOLEAN DEFAULT false
);

CREATE TABLE IF NOT EXISTS aquilo_user_session (
  "id" SERIAL PRIMARY KEY,
  "user_id" BIGINT  NOT NULL,
  "token" VARCHAR (1024) NOT NULL,
  "expiration_date" TIMESTAMP  NOT NULL,
  "session_data" TEXT
);

CREATE TABLE IF NOT EXISTS aquilo_device (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(256) NOT NULL,
  "serial" VARCHAR (64) NOT NULL,
  "type" VARCHAR (256) NOT NULL,
  "private_key" VARCHAR (1024) NOT NULL,
  "registered" TIMESTAMP WITHOUT TIME ZONE DEFAULT current_timestamp,
  "state" TEXT,
  "updated" TIMESTAMP WITHOUT TIME ZONE DEFAULT current_timestamp
);

CREATE TABLE IF NOT EXISTS aquilo_user_devices (
  "user_id" BIGINT references aquilo_user(id),
  "device_id" BIGINT references aquilo_device(id),
  "claimed" TIMESTAMP WITHOUT TIME ZONE DEFAULT current_timestamp,
  PRIMARY KEY ("user_id", "device_id")
);

CREATE TABLE IF NOT EXISTS aquilo_pending_action (
  "id" SERIAL PRIMARY KEY,
  "user_id" BIGINT references aquilo_user(id),
  "device_id" BIGINT references aquilo_device(id),
  "action" VARCHAR(255),
  "parameters" TEXT,
  "updated" TIMESTAMP WITHOUT TIME ZONE DEFAULT current_timestamp,
  "was_sent" BOOLEAN DEFAULT false
);

-- Generate Test Account
INSERT INTO aquilo_user ("id","email","name","user_token","hashed_password", "hash_count", "salt",  "permissions_level") VALUES (1,'aquilo@200bg.com','Aquilo Test','f65122f0-41eb-11e4-916c-0800200c9a66', 'hash', 2027, '03b26df0-41ec-11e4-916c-0800200c9a66', 256);


-- +goose Down
DROP TABLE aquilo_user;
DROP TABLE aquilo_client_type;
DROP TABLE aquilo_device;
DROP TABLE aquilo_user_devices;
DROP TABLE aquilo_pending_actions;

