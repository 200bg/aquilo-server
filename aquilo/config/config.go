package config

import (
	"code.google.com/p/gcfg"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net"
	"net/smtp"
	"os"
	"strings"
)

type Config struct {
	Server struct {
		Debug    bool
		Hostname string
		Port     int
		StaticUrl string
	}

	Sessions struct {
		Timeout int
		PrivateKey string // for later
	}

	Db struct {
		Hostaddress string
		Host        string
		Port        int
		User        string
		Password    string
		Name        string
		Timeout     int
		// generated from the above properties
		ConnectionString string
	}

	Errors struct {
		ErrorLog          string
		InfoLog           string
		Admins            string
		AdminEmailTo      []string
		EmailUseTls       string
		EmailUseSsl       string
		EmailHost         string
		EmailHostUser     string
		EmailHostPassword string
		EmailPort         string
		DefaultFromEmail  string
		SmtpAuth          smtp.Auth
	}
}

// USED EVERYWHERE
var GlobalConfig *Config

// NewConfig will load into GlobalConfig
// WHICH IS USED EVERYWHERE
// don't run this function blindly
func NewConfig(path string) *Config {
	cfg := new(Config)

	err := gcfg.ReadFileInto(cfg, path)
	if err != nil {
		fmt.Fprintln(os.Stdout, "Error parsing config files: %i", err)
	}

	// save the connection string
	if len(cfg.Db.Host) > 0 && len(cfg.Db.Hostaddress) == 0 {
		ipAddresses, err := net.LookupHost(cfg.Db.Host)
		if err != nil {
			log.Println("Couldn't lookup host", cfg.Db.Host, "\n", err)
		}
		if len(ipAddresses) > 0 {
			cfg.Db.Hostaddress = ipAddresses[0]
		}
	}
	cfg.Db.ConnectionString = fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s connect_timeout=%d sslmode=disable",
		cfg.Db.Hostaddress, cfg.Db.Port, cfg.Db.Name, cfg.Db.User, cfg.Db.Password, cfg.Db.Timeout)

	// setup email error logs
	smtpAuth := smtp.PlainAuth("", cfg.Errors.EmailHostUser, cfg.Errors.EmailHostPassword, cfg.Errors.EmailHost)
	cfg.Errors.SmtpAuth = smtpAuth

	cfg.Errors.AdminEmailTo = strings.Split(cfg.Errors.Admins, ",")

	// make this available app wide
	GlobalConfig = cfg
	return cfg
}

func (c *Config) ConnectToDb() (*sql.DB, error) {
	return sql.Open("postgres", c.Db.ConnectionString)
}
