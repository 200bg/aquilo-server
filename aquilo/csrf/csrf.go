package csrf
/*
Cross Site Request Forgery Middleware.

This module provides a middleware that implements protection
against request forgeries from other sites.
*/

// import logging
// import re

// from django.conf import settings
// from django.core.urlresolvers import get_callable
// from django.utils.cache import patch_vary_headers
// from django.utils.encoding import force_text
// from django.utils.http import same_origin
// from django.utils.crypto import constant_time_compare, get_random_string

import (
    "os"
    "time"
    "fmt"
	"log"
	"math/rand"
	"net/http"
	"crypto/md5"
	"crypto/subtle"
)

const REASON_NO_REFERER = "Referer checking failed - no Referer."
const REASON_BAD_REFERER = "Referer checking failed - %s does not match %s."
const REASON_NO_CSRF_COOKIE = "CSRF cookie not set."
const REASON_BAD_TOKEN = "CSRF token missing or incorrect."

const CSRF_KEY_LENGTH = 32


// // func _get_failure_view() {
// //     //Returns the view to be used for CSRF rejections
// //     return get_callable(settings.CSRF_FAILURE_VIEW)


func GenerateNewKey() string {
    now := time.Now().UnixNano()
    now = now + int64(os.Getpid())
    r := rand.New(rand.NewSource(now))
    seed := fmt.Sprintf("%d%d%d", r.Int63(), r.Int63(), r.Int63())
    key := md5.Sum([]byte(seed))
    return fmt.Sprintf("%x", key)
}

func GetToken(req *http.Request, burn bool) string {
    /*
    Returns the CSRF token required for a POST form. The token is an
    alphanumeric value.

    A side effect of calling this function is to make the csrf_protect
    decorator and the CsrfViewMiddleware add a CSRF cookie and a 'Vary: Cookie'
    header to the outgoing response.  For this reason, you may need to use this
    function lazily, as is done by the csrf context processor.
    */
    if burn {
        req.Header.Set("CSRF_COOKIE_USED", "1")
    }
    return req.Header.Get("CSRF_COOKIE")
}


func RotateToken(rw http.ResponseWriter, req *http.Request) string {
    /*
    Changes the CSRF token in use for a request - should be done on login
    for security purposes.
    */
    token := GenerateNewKey()
    rw.Header().Set("CSRF_COOKIE_USED", "0")
    rw.Header().Set("CSRF_COOKIE", token)
    return token
}

func IsCSRFableMethod(method string) bool {
    if (method != "GET" &&
       method != "HEAD" &&
       method != "OPTIONS" &&
       method != "TRACE") {
       return true
    } else {
        return false
    }
}

func isExempt(path string, exemptions []string) bool {
    for _, exemption := range exemptions {
        if exemption == path {
            return true
        }
    }
    return false
}

/*
Middleware that requires a present and correct csrfmiddlewaretoken
for POST requests that have a CSRF cookie, and sets an outgoing
CSRF cookie.

This middleware should be used in conjunction with the csrf_token template
tag.
*/
func Reject(req *http.Request, rw http.ResponseWriter, reason string) {
    log.Printf("Forbidden (%s): %s %d\n", reason, req.URL.Path, http.StatusForbidden)
    rw.WriteHeader(http.StatusForbidden)
    rw.Write([]byte(reason))
}

func ProcessView(h http.Handler, rw http.ResponseWriter, req *http.Request) bool {
    CSRFCookie, err := req.Cookie("csrfmiddlewaretoken")
    CSRFToken := ""
    if err == http.ErrNoCookie {
        CSRFToken = ""
        key := GenerateNewKey()
        // rw.Header().Set("CSRF_COOKIE", key)
        req.Header.Set("CSRF_COOKIE", key)
    } else {
        // Use same token next time
        CSRFToken = CSRFCookie.Value
        // rw.Header().Set("CSRF_COOKIE", CSRFToken)
        req.Header.Set("CSRF_COOKIE", CSRFToken)
    }

    // Wait until request.META["CSRF_COOKIE"] has been manipulated before
    // bailing out, so that GetToken still works
    // TODO: a way to mark a request csrf_exempt
    // if getattr(callback, 'csrf_exempt', False) {
    //     return None

    // Assume that anything not defined as 'safe' by RFC2616 needs protection
    if IsCSRFableMethod(req.Method) {
        // if getattr(request, '_dont_enforce_csrf_checks', False) {
        //     // Mechanism to turn off CSRF checks for test suite.
        //     // It comes after the creation of CSRF cookies, so that
        //     // everything else continues to work exactly the same
        //     // (e.g. cookies are sent, etc.), but before any
        //     // branches that call reject().
        //     return self.Accept(request)

        if req.TLS != nil {
            // Suppose user visits http://example.com/
            // An active network attacker (man-in-the-middle, MITM) sends a
            // POST form that targets https://example.com/detonate-bomb/ and
            // submits it via JavaScript.
            
            // The attacker will need to provide a CSRF cookie and token, but
            // that's no problem for a MITM and the session-independent
            // nonce we're using. So the MITM can circumvent the CSRF
            // protection. This is true for any HTTP connection, but anyone
            // using HTTPS expects better! For this reason, for
            // https://example.com/ we need additional protection that treats
            // http://example.com/ as completely untrusted. Under HTTPS,
            // Barth et al. found that the Referer header is missing for
            // same-domain requests in only about 0.2% of cases or less, so
            // we can use strict Referer checking.
            referer := req.Header.Get("HTTP_REFERER")
            if referer == "" {
                Reject(req, rw, REASON_NO_REFERER)
                return false
            }

            // Note that req.get_host() includes the port.
            goodReferer := fmt.Sprintf("https://%s/", req.Host)
            if referer != goodReferer {
                reason := fmt.Sprintf(REASON_BAD_REFERER, referer, goodReferer)
                Reject(req, rw, reason)
                return false
            }
        }

        if CSRFToken == "" {
            // No CSRF cookie. For POST reqs, we insist on a CSRF cookie,
            // and in this way we can avoid all CSRF attacks, including login
            // CSRF.
            Reject(req, rw, REASON_NO_CSRF_COOKIE)
            return false
        }

        // Check non-cookie token for match.
        requestCSRFToken := ""
        if req.Method == "POST" {
            requestCSRFToken = req.PostForm.Get("csrfmiddlewaretoken")
        }

        if requestCSRFToken == "" {
            // Fall back to X-CSRFToken, to make things easier for AJAX,
            // and possible for PUT/DELETE.
            requestCSRFToken = req.Header.Get("HTTP_X_CSRFTOKEN")
        }

        if subtle.ConstantTimeCompare([]byte(requestCSRFToken), []byte(CSRFToken)) != 1 {
            Reject(req, rw, REASON_BAD_TOKEN)
        }
    }
    return true
}

func ProcessResponse(h http.Handler, rw http.ResponseWriter, req *http.Request) {
    // TODO:
    // if getattr(response, 'csrf_processing_done', False) {
    //     return response

    // If CSRF_COOKIE is unset, then CsrfViewMiddleware.process_view was
    // never called, probably because a request middleware returned a response
    // (for example, contrib.auth redirecting to a login page).
    if req.Header.Get("CSRF_COOKIE") == "" {
        log.Println("No cookie, so can't set cookie")
        return
    }

    if req.Header.Get("CSRF_COOKIE_USED") == "1" {
        return
    }

    // Set the CSRF cookie even if it's already set, so we renew
    // the expiry timer.
    // TODO: provide a way to pull these from settings.
    cookie := http.Cookie {
        Name: "csrfmiddlewaretoken",
        Value: req.Header.Get("CSRF_COOKIE"),
        MaxAge: 31449600,
        Domain: req.URL.Host,
        Path: req.URL.Path,
        // Secure: False,
        // HttpOnly: False,
    }
    http.SetCookie(rw, &cookie)
}

func CSRFHandler(h http.Handler, rw http.ResponseWriter, req *http.Request) {
    isValid := ProcessView(h, rw, req)
    ProcessResponse(h, rw, req)
    if isValid == true {
        // pass through
        h.ServeHTTP(rw, req)
    } // else, the ProcessView should have taken over control of the rw
}

func CSRFMiddleware(h http.Handler, exemptions []string) http.Handler {
    return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
            if isExempt(req.URL.Path, exemptions) {
                h.ServeHTTP(rw, req)
                return
            }
            CSRFHandler(h, rw, req)
        })
}

