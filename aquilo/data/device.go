package data

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

type AquiloDevice struct {
	Id         int64
	Name       string
	Serial     string
	Type       string
	PrivateKey string
	Registered time.Time
	State      sql.NullString
	Updated    time.Time
}

type AquiloPendingAction struct {
	Id         int64
	UserId     int64
	DeviceId   int64
	Action     string
	Parameters sql.NullString
	Updated    time.Time
	WasSent    bool
	user       *AquiloUser
}

var DeviceDoesNotExist = errors.New("Device does not exist.")
var SuspiciousHashError = errors.New("Suspicious hash received.")
var DeviceAlreadyClaimed = errors.New("User has already claimed this device.")
var PermissionDenied = errors.New("User does not have permission to talk to this device.")

func GeneratePrivateKey(serial string) string {
	pid := os.Getpid()
	now := time.Now().UnixNano()
	r := rand.New(rand.NewSource(int64(pid)))
	randomNumber := r.Int63()

	seed := fmt.Sprintf("%s.%d.%d-%d", serial, now, pid, randomNumber)
	seedBuffer := []byte(seed)

	hasher := sha256.New()
	_, err := hasher.Write(seedBuffer)
	if err != nil {
		// this should panic and recover() should email an admin
		log.Panicln("Error hashing token.", err)
	}
	hash := hasher.Sum(nil)

	return hex.EncodeToString(hash)
}

func HashRequest(requestMethod string, device *AquiloDevice) string {
	seed := fmt.Sprintf("%s\t%s%s", requestMethod, device.PrivateKey, device.Serial)
	seedBuffer := []byte(seed)
	hasher := sha256.New()
	_, err := hasher.Write(seedBuffer)
	if err != nil {
		// this should panic and recover() should email an admin
		log.Panicln("Error hashing hashSeed.", err)
	}
	hash := hasher.Sum(nil)

	return hex.EncodeToString(hash)
}

func AquiloDeviceValidateRequest(db *sql.DB, serial string, requestMethod string, hash string) (*AquiloDevice, error) {
	device, _ := GetAquiloDeviceBySerial(db, serial)
	if device == nil {
		return nil, DeviceDoesNotExist
	}

	reconstructedHash := HashRequest(requestMethod, device)
	if reconstructedHash == hash {
		return device, nil
	}
	return nil, SuspiciousHashError
}

func DeleteAquiloDevice(db *sql.DB, serial string) bool {
	deleteSql := "delete from aquilo_device where serial = $1;"
	_, err := db.Exec(deleteSql, serial)
	if err != nil {
		return false
	} else {
		return true
	}
}

func GetAquiloDeviceIdBySerial(db *sql.DB, serial string) (int64, error) {
	var id int64
	selectSql := "select id from aquilo_device WHERE serial = $1 limit 1;"
	err := db.QueryRow(selectSql, serial).Scan(&id)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up device by serial.", err)
		return 0, err
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return 0, DeviceDoesNotExist
	}
	return id, nil
}

func GetAquiloDeviceBySerial(db *sql.DB, serial string) (*AquiloDevice, error) {
	d := new(AquiloDevice)
	selectSql := "select * from aquilo_device WHERE serial = $1 limit 1;"
	err := db.QueryRow(selectSql, serial).Scan(&d.Id, &d.Name, &d.Serial, &d.Type, &d.PrivateKey, &d.Registered, &d.State, &d.Updated)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up device by serial.", err)
		return nil, err
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return nil, err
	}
	return d, err
}

func AquiloDeviceUpdateState(db *sql.DB, serial string) (bool, error) {

	d, _ := GetAquiloDeviceBySerial(db, serial)
	if d == nil {
		return false, DeviceDoesNotExist
	}

	updateSql := "update aquilo_device set state = $1, updated = $2 where id = $3;"
	_, err := db.Exec(updateSql, d.State.String, d.Updated, d.Id)
	if err != nil {
		log.Println("Error saving device state. Failed to UPDATE device state.", err)
		return false, DatabaseUpdateError
	}

	return true, nil
}

func (d *AquiloDevice) Save(db *sql.DB) (int64, error) {
	if d.Id > 0 {
		// has Id, so we'll update.
		updateSql := "update aquilo_device set name = $1, " +
			"serial = $2, type = $3, private_key = $4, " +
			"registered = $5, state = $6, updated = $7 " +
			"where id = $8::bigint;"
		_, err := db.Exec(updateSql, d.Name, d.Serial, d.Type, d.PrivateKey, d.Registered, d.State.String, d.Updated, d.Id)
		if err != nil {
			log.Println("Error saving device. Failed to UPDATE device.", err)
			return d.Id, DatabaseUpdateError
		}
	} else {
		// no Id, insert it.
		insertSql := "insert into aquilo_device (name, " +
			"serial, type, private_key, registered, state, updated) " +
			"values " +
			"($1, $2, $3, $4, $5, $6, $7) " +
			"returning id;"
		err := db.QueryRow(insertSql, d.Name, d.Serial, d.Type, d.PrivateKey, d.Registered, d.State.String, d.Updated).Scan(&d.Id)
		if err != nil {
			log.Println("Error saving device. Failed to INSERT device.", err)
			return 0, DatabaseInsertError
		}
	}

	return d.Id, nil
}

func (d *AquiloDevice) UpdateState(db *sql.DB) (bool, error) {
	updateSql := "update aquilo_device set state = $1, updated = $2 where id = $3;"
	_, err := db.Exec(updateSql, d.State.String, d.Updated, d.Id)
	if err != nil {
		log.Println("Error saving device state. Failed to UPDATE device state.", err)
		return false, DatabaseUpdateError
	}
	return true, nil
}

func (d *AquiloDevice) GetPendingAction(db *sql.DB) *AquiloPendingAction {
	a := new(AquiloPendingAction)
	selectSql := "select * from aquilo_pending_action where device_id = $1::bigint and updated > now() - interval '5 minutes' order by updated desc limit 1;"
	err := db.QueryRow(selectSql, d.Id).Scan(&a.Id, &a.UserId, &a.DeviceId, &a.Action, &a.Parameters, &a.Updated, &a.WasSent)
	if err != nil {
		log.Println("Error looking up actions.", err)
		return nil
	}
	return a
}

func ClaimDevice(db *sql.DB, u *AquiloUser, serial string) error {
	// does the user already have this claimed?
	var count int64
	selectSql := "select count(id) from aquilo_user_devices inner join aquilo_device on aquilo_device.id = aquilo_user_devices.device_id where aquilo_device.serial = $1 and aquilo_user_devices.user_id = $2;"
	err := db.QueryRow(selectSql, serial, u.Id).Scan(&count)
	if err != nil {
		log.Println("Error looking up device.", err)
		return DatabaseQueryError
	}

	if count > 0 {
		return DeviceAlreadyClaimed
	}

	deviceId, _ := GetAquiloDeviceIdBySerial(db, serial)

	insertSql := "insert aquilo_user_devices (user_id, device_id, claimed) values ($1::bigint, $2::bigint, now());"
	_, err = db.Exec(insertSql, u.Id, deviceId)
	if err != nil {
		log.Println("Error claiming device.", err)
		return DatabaseInsertError
	}
	return nil
}

func HasUserClaimedDevice(db *sql.DB, userId int64, deviceId int64) (bool, error) {
	var count int64
	selectSql := "select count(id) from aquilo_user_devices where user_id = $1::bigint and device_id = $2::bigint;"
	err := db.QueryRow(selectSql, userId, deviceId).Scan(&count)
	if err != nil {
		log.Println("Error looking up device.", err)
		return false, DatabaseQueryError
	}

	return count > 0, nil
}

func DisownDevice(db *sql.DB, userId int64, deviceId int64) bool {
	deleteSql := "delete from aquilo_user_devices where userId = $1::bigint and deviceId = $2::bigint;"
	_, err := db.Exec(deleteSql, userId, deviceId)
	if err != nil {
		return false
	} else {
		return true
	}
}

func (a *AquiloPendingAction) GetUser(db *sql.DB) *AquiloUser {
	// cached?
	if a.user != nil {
		return a.user
	}
	a.user = GetAquiloUserById(db, a.UserId)
	return a.user
}

func (a *AquiloPendingAction) Save(db *sql.DB) (int64, error) {
	// check if this user has permission to
	hasPermission, _ := HasUserClaimedDevice(db, a.UserId, a.DeviceId)

	if !hasPermission {
		return 0, PermissionDenied
	}

	a.Updated = time.Now()
	if a.Id > 0 {
		// has Id, so we'll update.
		updateSql := "update aquilo_pending_action set user_id = $1::bigint, " +
			"device_id = $2::bigint, action = $3, parameters = $4, " +
			"was_sent = $5, updated = $6 " +
			"where id = $7::bigint;"
		_, err := db.Exec(updateSql, a.UserId, a.DeviceId, a.Action, a.Parameters, a.WasSent, a.Updated, a.Id)
		if err != nil {
			log.Println("Error saving action. Failed to UPDATE action.", err)
			return a.Id, DatabaseUpdateError
		}
	} else {
		// no Id, insert it.
		insertSql := "insert into aquilo_pending_action (user_id, " +
			"device_id, action, parameters, was_sent, updated) " +
			"values " +
			"($1::bigint, $2::bigint, $3, $4, $5, $6) " +
			"returning id;"
		err := db.QueryRow(insertSql, a.UserId, a.DeviceId, a.Action, a.Parameters, a.WasSent, a.Updated).Scan(&a.Id)
		if err != nil {
			log.Println("Error saving action. Failed to INSERT action.", err)
			return 0, DatabaseInsertError
		}
	}

	return a.Id, nil
}

func (a *AquiloPendingAction) Delete(db *sql.DB) bool {
	deleteSql := "delete from aquilo_pending_action where id = $1::bigint;"
	_, err := db.Exec(deleteSql, a.Id)
	if err != nil {
		return false
	} else {
		return true
	}
}
