package data

import (
	"bitbucket.org/200bg/aquilo-server/aquilo/config"
	"code.google.com/p/go-uuid/uuid"
	"code.google.com/p/go.crypto/pbkdf2"
	"container/list"
	"crypto/sha256"
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"errors"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

// AquiloUser corresponds with table aquilo_user
type AquiloUser struct {
	Id               int64
	Email            string
	Name             string
	UserToken        string
	HashedPassword   sql.NullString
	Salt             string
	HashCount        int64
	PermissionsLevel int64
	IsBanned         bool
	// array of devices
	Devices list.List
}

type AquiloUserSession struct {
	Id             int64
	UserId         int64
	Token          string
	ExpirationDate time.Time
	SessionData    sql.NullString
}

// a const that's not a primitive... is this even okay?
var AQUILO_VALID_USERNAME_CHARACTERS = regexp.MustCompile("(?i)^[a-zA-Z0-9äÄöÖüÜßáéíóúñ@\\._\\-\\+]+$")
var AQUILO_VALID_PASSWORD_NUMBERS = regexp.MustCompile("[0-9]")
var AQUILO_VALID_PASSWORD_ALPHAS = regexp.MustCompile("[a-zA-ZäÄöÖüÜßáéíóúñ]")

const AQUILO_USER_PERMISSIONS_SUPER int64 = 255
const AQUILO_USER_PERMISSIONS_MANAGER int64 = 128
const AQUILO_USER_PERMISSIONS_STAFF int64 = 64
const AQUILO_USER_PERMISSIONS_COMMUNITY int64 = 16
const AQUILO_USER_PERMISSIONS_MODERATOR int64 = 4
const AQUILO_USER_PERMISSIONS_STANDARD int64 = 0

var BannedUserError = errors.New("User is banned.")
var UserTokenError = errors.New("Error creating user token.")
var SaltError = errors.New("Error creating user salt.")
var BadSessionError = errors.New("Invalid session requested.")

var BadUsernameError = errors.New("Invalid email provided.")
var ExistingUsernameError = errors.New("Email is already registered.")
var BadPasswordError = errors.New("Invalid password provided.")
var DatabaseQueryError = errors.New("Error querying the database.")
var DatabaseInsertError = errors.New("Error inserting into the database.")
var DatabaseUpdateError = errors.New("Error updating the database.")

func ValidateUsername(db *sql.DB, username string) error {
	// no username? what?
	// is it at least 8?
	if len(username) < 8 || len(username) > 65 {
		return BadUsernameError
	}

	// check the valid characters
	if !AQUILO_VALID_USERNAME_CHARACTERS.MatchString(username) {
		return BadUsernameError
	}

	// now, does the username exist?
	countSql := "select count(id) as count from aquilo_user where email = $1;"
	count := 0
	err := db.QueryRow(countSql, username).Scan(&count)
	if err != nil {
		log.Println("Failed to check for uniqueness of email.", username, err)
		return DatabaseQueryError
	}

	if count > 0 {
		return ExistingUsernameError
	}
	return nil
}

func ValidatePassword(username string, rawPassword string) error {
	passwordLength := len(rawPassword)
	// check the length requirements
	if passwordLength < 8 || passwordLength > 255 {
		return BadPasswordError
	}

	lowercaseUsername := strings.ToLower(username)
	lowercasePassword := strings.ToLower(rawPassword)
	// you can't use the same thing twice
	if lowercaseUsername == lowercasePassword {
		return BadPasswordError
	}

	// has it a number?
	numberLocations := AQUILO_VALID_PASSWORD_NUMBERS.FindAllStringIndex(rawPassword, 256)
	// but, like, is it all numbers?
	if len(numberLocations) == len(rawPassword) {
		// there are only numbers
		return BadPasswordError
	}

	// require non alpha (numbers count)
	alphaLocations := AQUILO_VALID_PASSWORD_ALPHAS.FindAllStringIndex(rawPassword, 256)
	if len(alphaLocations) == len(rawPassword) {
		// there are no non-alphas!
		return BadPasswordError
	}

	return nil
}

func AquiloUserAuthenticate(db *sql.DB, username string, rawPassword string) (*AquiloUser, error) {
	if len(username) > 65 {
		return nil, BadUsernameError
	}

	u := GetAquiloUserByUsername(db, username)
	if u == nil {
		return nil, BadUsernameError
	}

	if len(u.Salt) < 128 {
		log.Println("Error during authentication; Invalid salt found for user", username)
		return u, DatabaseQueryError
	}

	// legacy hash
	hashedPassword := HashPassword(rawPassword, u.Salt, u.HashCount)

	if hashedPassword == u.HashedPassword.String {
		// only tell them they're banned if they remember their password
		if u.IsBanned {
			return u, BannedUserError
		} else {
			return u, nil
		}
	}

	return nil, DatabaseQueryError
}

func DeleteAquiloUser(db *sql.DB, username string) bool {
	if len(username) > 64 {
		return false
	}

	deleteSql := "delete from aquilo_user where email = $1;"
	_, err := db.Exec(deleteSql, username)
	if err != nil {
		return false
	} else {
		return true
	}
}

func HashPassword(rawPassword string, salt string, hashCount int64) string {
	dk := pbkdf2.Key([]byte(rawPassword), []byte(salt), int(hashCount), 256, sha256.New)

	return hex.EncodeToString(dk)
}

func GenerateUserToken(db *sql.DB) string {
	userToken := uuid.New()
	countSql := "select count(id) from aquilo_user where user_token = $1;"
	count := 0
	err := db.QueryRow(countSql, userToken).Scan(&count)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking for duplicate uuid's.", err)
		return ""
	}
	if count > 0 {
		// could recurse forever, but with very very low odds.
		return GenerateUserToken(db)
	}
	return userToken
}

// GetAquiloUserIDByUsername returns int64 by username
func GetAquiloUserIDByUsername(db *sql.DB, username string) int64 {
	selectSql := "select id from aquilo_user WHERE email = $1 limit 1;"
	id := int64(0)
	err := db.QueryRow(selectSql, username).Scan(&id)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up id by email.", err)
		return 0
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return 0
	}
	return id
}

// GetAquiloUserByUsername returns *AquiloUser by username
func GetAquiloUserByUsername(db *sql.DB, username string) *AquiloUser {
	u := new(AquiloUser)
	selectSql := "select * from aquilo_user WHERE email = $1 limit 1;"
	err := db.QueryRow(selectSql, username).Scan(&u.Id, &u.Email, &u.Name, &u.UserToken, &u.HashedPassword, &u.Salt, &u.HashCount, &u.PermissionsLevel, &u.IsBanned)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up user by email.", err)
		return nil
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return nil
	}
	return u
}

// GetAquiloUserByUserToken returns *AquiloUser by token
func GetAquiloUserByUserToken(db *sql.DB, userToken string) *AquiloUser {
	u := new(AquiloUser)
	selectSql := "select * from aquilo_user WHERE user_token = $1 limit 1;"
	err := db.QueryRow(selectSql, userToken).Scan(&u.Id, &u.Email, &u.Name, &u.UserToken, &u.HashedPassword, &u.Salt, &u.HashCount, &u.PermissionsLevel, &u.IsBanned)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up user by user_token.", err)
		return nil
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return nil
	}
	return u
}

// GetAquiloUserById returns *AquiloUser by token
func GetAquiloUserById(db *sql.DB, userId int64) *AquiloUser {
	u := new(AquiloUser)
	selectSql := "select * from aquilo_user WHERE id = $1;"
	err := db.QueryRow(selectSql, userId).Scan(&u.Id, &u.Email, &u.Name, &u.UserToken, &u.HashedPassword, &u.Salt, &u.HashCount, &u.PermissionsLevel, &u.IsBanned)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up user by id.", err)
		return nil
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return nil
	}
	return u
}

func AquiloUserExists(db *sql.DB, username string) bool {
	selectSql := "select id from aquilo_user WHERE email = $1 limit 1;"
	id := 0
	err := db.QueryRow(selectSql, username).Scan(&id)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up id by email.", err)
		return false
	} else if err == sql.ErrNoRows {
		// the client either doesn't exist or just isn't valid
		return false
	}
	return id != 0
}

// TODO: return error, not bool
func AquiloUserPermit(db *sql.DB, username string, permissionsLevel int64) bool {
	if permissionsLevel > 255 {
		permissionsLevel = 255
	}

	if len(username) > 65 || len(username) == 0 {
		return false
	}

	updateSql := "update aquilo_user set permissions_level = $1 where email = $2;"
	_, err := db.Exec(updateSql, permissionsLevel, username)
	if err != nil {
		log.Println("Error permitting user. Failed to UPDATE user.", err)
		return false
	}

	return true
}

func AquiloUserChangePassword(db *sql.DB, email string, newPassword string, hashCount int64) (*AquiloUser, error) {
	passwordValidation := ValidatePassword(email, newPassword)
	if passwordValidation != nil {
		return nil, passwordValidation
	}

	salt := GenerateSalt()
	hashedPassword := HashPassword(newPassword, salt, hashCount)

	updateSql := "update aquilo_user set salt = $1, hashed_password = $2, " +
		"hash_count = $3::int where email = $4;"
	_, err := db.Exec(updateSql, salt, hashedPassword, hashCount, email)
	if err != nil {
		if config.GlobalConfig.Server.Debug {
			log.Println(err)	
		}
		return nil, DatabaseUpdateError
	}

	return AquiloUserAuthenticate(db, email, newPassword)
}

func AquiloUserChangeEmail(db *sql.DB, oldEmail string, email string) (*AquiloUser, error) {
	// validate the username
	validation := ValidateUsername(db, email)
	if validation != nil {
		return nil, validation
	}

	updateSql := "update aquilo_user set email = $1 where email = $2;"
	_, err := db.Exec(updateSql, email, oldEmail)
	if err != nil {
		return nil, DatabaseUpdateError
	}
	u := GetAquiloUserByUsername(db, email)
	if u == nil {
		return nil, DatabaseUpdateError
	}
	return u, nil
}

func AquiloUserSetBan(db *sql.DB, ban bool, email string) bool {
	updateSql := "update aquilo_user set is_banned = $1 where email = $2;"
	banInt := 0
	if ban {
		banInt = 1
	}
	_, err := db.Exec(updateSql, banInt, email)
	if err != nil {
		log.Println("Error banning user. Failed to UPDATE user.", err)
		return false
	}
	return true
}

// Save returns the id, and a result code
func (u *AquiloUser) Save(db *sql.DB) (int64, error) {
	// last chance to lowercase
	u.Email = strings.ToLower(u.Email)
	// assumes valid username until told otherwise

	// also validate the username if an insert
	if u.Id <= 0 {
		validation := ValidateUsername(db, u.Email)
		if validation != nil {
			return 0, validation
		}
	}

	// make sure we have a token
	if len(u.UserToken) < 36 {
		log.Println("Error creating user.", u.Email, "No user-token supplied.")
		return 0, UserTokenError
	}

	if len(u.HashedPassword.String) < 128 {
		log.Println("Error creating user.", u.Email, "No password supplied.")
		return 0, BadPasswordError
	}

	if len(u.Salt) < 128 {
		log.Println("Error creating user.", u.Email, "No password salt supplied.")
		return 0, SaltError
	}

	bannedInt := 0
	if u.IsBanned {
		bannedInt = 1
	}

	if u.Id > 0 {
		// has Id, so we'll update.
		updateSql := "update aquilo_user set email = $1, name = $2 " +
			"user_token = $3, hashed_password = $4, salt = $5, " +
			"hash_count = $6::int, permissions_level = $7::int, is_banned = $8::bool " +
			"where id = $9::bigint;"
		_, err := db.Exec(updateSql, u.Email, u.Name, u.UserToken, u.HashedPassword.String, u.Salt, u.HashCount, u.PermissionsLevel, bannedInt, u.Id)
		if err != nil {
			log.Println("Error saving user. Failed to UPDATE user.", err)
			return u.Id, DatabaseUpdateError
		}
	} else {
		// no Id, insert it.
		insertSql := "insert into aquilo_user (email, name, " +
			"user_token, hashed_password, salt, hash_count, permissions_level," +
			"is_banned) values " +
			"($1, $2, $3, $4, $5, $6::int, $7::int, $8::bool) " +
			"returning id;"
		err := db.QueryRow(insertSql, u.Email, u.Name, u.UserToken, u.HashedPassword.String, u.Salt, u.HashCount, u.PermissionsLevel, bannedInt).Scan(&u.Id)
		if err != nil {
			log.Println("Error saving user. Failed to INSERT user.", err)
			return 0, DatabaseInsertError
		}
	}

	return u.Id, nil
}

func (u *AquiloUser) GetDevices(db *sql.DB) list.List {
	if u.Devices.Len() == 0 {
		selectSql := "select device.* from aquilo_device device inner join aquilo_user on aquilo_user.id = device.user_id WHERE aquilo_user.id = $1;"
		rows, err := db.Query(selectSql, u.Id)
		if err != nil && err != sql.ErrNoRows {
			log.Println("Error looking up devices.", err)
			return u.Devices
		} else if err == sql.ErrNoRows {
			return u.Devices
		}

		defer rows.Close()
		for rows.Next() {
			d := new(AquiloDevice)
			err = rows.Scan(&d.Id, &d.Name, &d.Serial, &d.PrivateKey, &d.Registered, &d.State, &d.Updated)
			if err == nil {
				u.Devices.PushBack(d)
			}
		}
	}
	return u.Devices
}

func (u *AquiloUser) SetBan(db *sql.DB, ban bool) bool {
	updateSql := "update aquilo_user set is_banned = $1 where email = $2;"
	_, err := db.Exec(updateSql, ban, u.Email)
	if err != nil {
		log.Println("Error banning user. Failed to UPDATE user.", err)
		return false
	}

	return true
}

func (u *AquiloUser) Ban(db *sql.DB) bool {
	return u.SetBan(db, true)
}

func (u *AquiloUser) UnBan(db *sql.DB) bool {
	return u.SetBan(db, false)
}

// Session stuff

func NewAquiloUserSession(user *AquiloUser) *AquiloUserSession {
	// if you sent bogus stuff, I can't create the session.
	if user == nil || user.Id == 0 {
		return nil
	}

	s := new(AquiloUserSession)
	s.UserId = user.Id
	s.ExpirationDate = time.Now().UTC().Add(time.Duration(config.GlobalConfig.Sessions.Timeout) * time.Second)

	return s
}

func LookUpAquiloUserSession(db *sql.DB, token string) *AquiloUserSession {
	s := new(AquiloUserSession)
	selectSql := "select * from aquilo_user_session WHERE token = $1;"
	err := db.QueryRow(selectSql, token).Scan(&s.Id, &s.UserId, &s.Token, &s.ExpirationDate, &s.SessionData)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up session.", err)
		return nil
	} else if err == sql.ErrNoRows {
		// the token either doesn't exist
		return nil
	}
	return s
}

func ValidateAquiloUserSession(db *sql.DB, userToken string, sessionToken string) (*AquiloUserSession, error) {
	s := new(AquiloUserSession)
	selectSql := "select session.* from aquilo_user_session session inner join aquilo_user on aquilo_user.id = session.user_id WHERE aquilo_user.user_token = $1 and session.token = $2 and cast(session.expiration_date at time zone 'utc' as date) > now() limit 1"
	err := db.QueryRow(selectSql, userToken, sessionToken).Scan(&s.Id, &s.UserId, &s.Token, &s.ExpirationDate, &s.SessionData)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up session.", err)
		return nil, DatabaseQueryError
	} else if err == sql.ErrNoRows {
		// the token either doesn't exist, or is expired
		return nil, BadSessionError
	} else {
		return s, nil
	}
	// should never reach here, actually
	return nil, BadSessionError
}

func ValidateAquiloUserSessionAndGetUserFromRequest(db *sql.DB, req *http.Request) (*AquiloUserSession, *AquiloUser) {
	sessionToken, err := req.Cookie("sessionid")
	if sessionToken != nil && err == nil && sessionToken.Value != "" {
		return ValidateAquiloUserSessionAndGetUser(db, sessionToken.Value)
	} else {
		return nil, nil	
	}
}

func ValidateAquiloUserSessionAndGetUser(db *sql.DB, sessionToken string) (*AquiloUserSession, *AquiloUser) {
	u := new(AquiloUser)
	selectSql := "select aquilo_user.* from aquilo_user inner join aquilo_user_session session on aquilo_user.id = session.user_id WHERE session.token = $1 and cast(session.expiration_date at time zone 'utc' as date) > now()"
	err := db.QueryRow(selectSql, sessionToken).Scan(&u.Id, &u.Email, &u.Name, &u.UserToken, &u.HashedPassword, &u.Salt, &u.HashCount, &u.PermissionsLevel, &u.IsBanned)
	if err != nil && err != sql.ErrNoRows {
		log.Println("Error looking up user from session.", err)
		return nil, nil
	} else if err == sql.ErrNoRows {
		// the token either doesn't exist
		return nil, nil
	} else {
		// when validated extend.
		s, _ := ValidateAquiloUserSession(db, u.UserToken, sessionToken)
		s.ExpirationDate = time.Now().UTC().Add(time.Duration(config.GlobalConfig.Sessions.Timeout) * time.Second)
		s.Save(db)
		return s, u
	}
	// should never read here, actually
	return nil, nil
}

func GenerateSessionKey(email string, tryCount int64) string {
	pid := os.Getpid()
	now := time.Now().UnixNano()
	tryCount += 1

	seed := fmt.Sprintf("%s.%d.%d-%d", email, now, pid, tryCount)
	seedBuffer := []byte(seed)

	hasher := sha512.New()
	_, err := hasher.Write(seedBuffer)
	if err != nil {
		// this should panic and recover() should email an admin
		log.Panicln("Error hashing token.", err)
	}
	hash := hasher.Sum(nil)

	return hex.EncodeToString(hash)
}

func GenerateSalt() string {
	pid := os.Getpid()
	now := time.Now().UnixNano()
	seed := fmt.Sprintf("%d.%d", now, pid)
	seedBuffer := []byte(seed)

	hasher := sha512.New()
	_, err := hasher.Write(seedBuffer)
	if err != nil {
		// this should panic and recover() should email an admin
		log.Panicln("Error hashing password.", err)
	}
	hash := hasher.Sum(nil)

	return hex.EncodeToString(hash)
}

func GenerateSessionToken(db *sql.DB, email string, tryCount int64) (string, error) {
	// in crypto.go
	tokenString := GenerateSessionKey(email, tryCount)
	countSql := "select count(id) from aquilo_user_session where token = $1;"
	count := 0
	err := db.QueryRow(countSql, tokenString).Scan(&count)
	if err != nil {
		log.Println("Failed to check for uniqueness of session token.", tokenString, err)
		return "", UserTokenError
	}

	if count > 0 {
		tryCount += 1
		if tryCount < 255 {
			return GenerateSessionToken(db, email, tryCount)
		} else {
			log.Println("Too many failed uniqueness checks session token.", email)
			return "", DatabaseQueryError
		}
	}
	return tokenString, nil
}

func (s *AquiloUserSession) Save(db *sql.DB) (int64, error) {
	if s.UserId <= 0 {
		log.Println("No user id provided for session.")
		return 0, BadUsernameError
	}
	
	if len(s.Token) == 0 {
		if s.Id == 0 {
			// it's a new session, so create a token
			u := s.GetUser(db)
			token, err := GenerateSessionToken(db, u.Email, 0)
			if err != nil {
				return 0, err	
			}
			s.Token = token
		} else {
			log.Println("No token was provided for session.")
			return 0, UserTokenError
		}
	}

	if s.Id > 0 {
		// has Id, so we'll update.
		updateSql := "update aquilo_user_session set token = $1, " +
			"user_id = $2::int, " +
			"expiration_date = $3::timestamp without time zone, session_data = $4 " +
			"where id = $5::bigint;"
		_, err := db.Exec(updateSql, s.Token, s.UserId, s.ExpirationDate, s.SessionData, s.Id)
		if err != nil {
			log.Println("Error saving session. Failed to UPDATE session.", err)
			return s.Id, DatabaseUpdateError
		}
	} else {
		// no Id, insert it.
		insertSql := "insert into aquilo_user_session (token, " +
			"user_id, expiration_date, session_data) " +
			"values ($1, $2::int, $3::timestamp without time zone, $4) " +
			"returning id;"
		err := db.QueryRow(insertSql, s.Token, s.UserId, s.ExpirationDate, s.SessionData).Scan(&s.Id)
		if err != nil {
			log.Println("Error saving session. Failed to INSERT session.", err)
			return 0, DatabaseInsertError
		}
	}

	return s.Id, nil
}

func (s *AquiloUserSession) GetUser(db *sql.DB) *AquiloUser {
	return GetAquiloUserById(db, s.UserId)
}

// detele one particular session token
func DeleteUserSession(db *sql.DB, sessionToken string) bool {
	deleteSql := "delete from aquilo_user_session where token = $1;"
	_, err := db.Exec(deleteSql, sessionToken)
	if err != nil {
		return false
	} else {
		return true
	}
}

// delete all session tokens
func DeleteAllUserSessions(db *sql.DB, email string) bool {
	if len(email) > 64 {
		return false
	}

	deleteSql := "delete from aquilo_user_session session using aquilo_user where aquilo_user.id = session.user_id and aquilo_user.email = $1;"
	_, err := db.Exec(deleteSql, email)
	if err != nil {
		return false
	} else {
		return true
	}
}
