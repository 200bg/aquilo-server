package aquilo

import (
	"bitbucket.org/200bg/aquilo-server/aquilo/config"
	"bitbucket.org/200bg/aquilo-server/aquilo/data"
	"bitbucket.org/200bg/aquilo-server/aquilo/csrf"
	"container/list"
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"time"
)

const VERSION = "0.0.0"

var views = new(template.Template)

type DeviceState struct {
	Temperature         float32
	Humidity            float32
	TargetTemperature   float32
	Overridden          bool
	OverrideTemperature float32
	Mode                int
	RunningTime         int
}

type RegistrationMessage struct {
	Serial string
	Name   string
	Type   string
	ApiKey string
	State  *DeviceState
}

// type ResponseError struct {
// 	Code    int
// 	Message string
// }

type ResponseAction struct {
	Action     string
	Parameters string
	RequestBy  string
	When       time.Time
}

type ResponseMessageString struct {
	Result        string
	Error         string
	PendingAction *ResponseAction
}

type ResponseMessageDevice struct {
	Result        data.AquiloDevice
	Error         string
	PendingAction *ResponseAction
}

func LoadTemplates() {
	views, _ = template.ParseGlob("./html/*.html")
}

func ExecuteTemplate(rw http.ResponseWriter, name string, data interface{}) error {
	if config.GlobalConfig.Server.Debug {
		views, _ = template.ParseGlob("./html/*.html")
	}
	err := views.ExecuteTemplate(rw, name, data)
	if err != nil {
		log.Printf("Error parsing %s template.\n", name)
	}
	return err
}

func validateRequest(method string, req *http.Request) (*sql.DB, *data.AquiloDevice, error) {
	conn, err := config.GlobalConfig.ConnectToDb()
	hash := req.Header.Get("X-Hash")
	serial := req.Header.Get("X-Serial")
	device, err := data.AquiloDeviceValidateRequest(conn, serial, method, hash)
	return conn, device, err
}

func HomeHandler(rw http.ResponseWriter, req *http.Request) {
	log.Println("GET /")
	conn, _ := config.GlobalConfig.ConnectToDb()
	session, user := data.ValidateAquiloUserSessionAndGetUserFromRequest(conn, req)
	
	type HomeValues struct {
		Settings *config.Config
		Request  *http.Request
		CSRFToken string
		Errors    *list.List
	}
	
	context := HomeValues {
		Settings: config.GlobalConfig,
		Request: req,
		CSRFToken: csrf.RotateToken(rw, req),
		Errors: new(list.List),
	}
	
	if session != nil && user != nil {
		// we're good
		ExecuteTemplate(rw, "index.html", context)
	} else {
		// no session, force login
		http.Redirect(rw, req, "/login/", 302)
	}
	
}

func LoginHandler(rw http.ResponseWriter, req *http.Request) {
	log.Println("GET /login/");
	
	type LoginValues struct {
		Settings *config.Config
		Request  *http.Request
		Form  url.Values
		Username  string
		Password  string
		CSRFToken string
		Errors    []error
	}
	
	context := LoginValues {
		Settings: config.GlobalConfig,
		Request: req,
		Form: req.PostForm,
		Username: req.PostFormValue("username"),
		Password: req.PostFormValue("password"),
		CSRFToken: csrf.RotateToken(rw, req),
	}
	
	// if it's a post, login
	if req.Method != "GET" {
		conn, err := config.GlobalConfig.ConnectToDb()
		defer conn.Close()
		if err != nil {
			log.Println(err)
		}
		if conn != nil {
			// aquilo sanitizes these during auth, as well as go's templates
			user, err := data.AquiloUserAuthenticate(conn, req.PostFormValue("username"), req.PostFormValue("password"))
			if err != nil {
				context.Errors = append(context.Errors, err)
			} else {
				// user is good, create a session
				session := data.NewAquiloUserSession(user)
				session.Save(conn)
				
				// after it is saved, we'll have a token, store it in a cookie
				now := time.Now().Unix()
				cookie := http.Cookie {
			        Name: "sessionid",
			        Value: session.Token,
			        MaxAge: int(session.ExpirationDate.Unix() - now),
			        Domain: req.URL.Host,
			        Path: "/",
			        // Secure: False,
			        // HttpOnly: False,
			    }
			    http.SetCookie(rw, &cookie)
			    req.AddCookie(&cookie)
    
				// session is in there, lets redirect to index
				http.Redirect(rw, req, "/", 302)
			}
		}
	}
	// if it's just a get then display the form.
	ExecuteTemplate(rw, "login.html", context)
}

// RegisterDeviceHandler
// One of the few POST handlers that doesn't validate the hash
func RegisterDeviceHandler(rw http.ResponseWriter, req *http.Request) {
	conn, err := config.GlobalConfig.ConnectToDb()
	defer conn.Close()

	if err != nil {
		log.Println(err)
	}
	if conn != nil {
		decoder := json.NewDecoder(req.Body)
		var reg RegistrationMessage
		err := decoder.Decode(&reg)
		if err != nil {
			log.Println("Error parsing registration.")
		} else {
			// first see if the device is registered,
			d, err := data.GetAquiloDeviceBySerial(conn, reg.Serial)
			if (d != nil) {
				log.Printf("Device %s : %s came online.", reg.Name, reg.Serial)
				// the state should have come through
				d.Updated = time.Now()
				jsonState, _ := json.Marshal(reg.State)
				d.State.String = string(jsonState)
				_, err = d.Save(conn)
			} else {
				// if not, register the device.
				d := new(data.AquiloDevice)
				d.Name = reg.Name
				d.Serial = reg.Serial
				d.Type = reg.Type
				d.PrivateKey = data.GeneratePrivateKey(reg.Serial)
				d.Registered = time.Now()
				d.Updated = time.Now()
				if reg.State != nil {
					jsonState, err := json.Marshal(reg.State)
					if err != nil {
						log.Println("Invalid state provided during registration.")
						d.State.String = "{}"
					} else {
						d.State.String = string(jsonState)
					}
				}
				_, err = d.Save(conn)
				log.Printf("A new %s device registered: %s : %s", reg.Type, reg.Name, reg.Serial)
			}
			
			if err != nil {
				log.Println("Error saving device.")
				response := new(ResponseMessageString)
				response.Error = err.Error()
				response.Result = ""
				response.PendingAction = nil
				jsonResponse, _ := json.Marshal(response)
				http.Error(rw, string(jsonResponse), http.StatusInternalServerError)
			} else {
				response := new(ResponseMessageDevice)
				response.Error = ""
				response.Result = *d
				response.PendingAction = nil
				jsonResponse, _ := json.Marshal(response)
				
				fmt.Fprintf(rw, "%s", string(jsonResponse))
			}
		}
		conn.Close()
	} else {
		log.Println("Error parsing device.")
		http.Error(rw, "Error parsing device.", http.StatusInternalServerError)
	}
}

func VitalsHandler(rw http.ResponseWriter, req *http.Request) {
	conn, device, err := validateRequest("vitals", req)
	defer conn.Close()

	if err != nil {
		http.Error(rw, "", http.StatusBadRequest)
	} else {
		// good hash, let's save the vitals
		decoder := json.NewDecoder(req.Body)
		var state DeviceState
		err := decoder.Decode(&state)
		if err != nil {
			log.Println("Invalid state provided.")
			http.Error(rw, "Unable to decode state.", http.StatusInternalServerError)
		} else {
			jsonState, err := json.Marshal(state)
			if err != nil {
				log.Println("Unable to save state.")
				http.Error(rw, "Unable to save state.", http.StatusInternalServerError)
			} else {
				saveableState := string(jsonState)
				device.State.String = saveableState
				device.UpdateState(conn)
				pendingAction := device.GetPendingAction(conn)

				response := new(ResponseMessageString)
				response.Error = ""
				response.Result = "success"
				if pendingAction == nil {
					response.PendingAction = nil
				} else {
					responseAction := new(ResponseAction)
					responseAction.Action = pendingAction.Action
					responseAction.Parameters = pendingAction.Parameters.String
					responseAction.RequestBy = pendingAction.GetUser(conn).Name
					responseAction.When = pendingAction.Updated

					response.PendingAction = responseAction
				}
				jsonResponse, _ := json.Marshal(response)

				fmt.Fprintf(rw, "%s", string(jsonResponse))
			}
		}
	}
}

// ConfigureHandler
// Just loads the html template to take the user through configuring their device.
func ConfigureHandler(rw http.ResponseWriter, req *http.Request) {

}
