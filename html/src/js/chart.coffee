window.ccmm = window.ccmm || {}

TAU = Math.PI*2
CHART_START_ANGLE = TAU/-2

class ccmm.Chart
  constructor: (@element, @dataArray, @theme, @colors, @options={}) ->
    @options = @options or {}

    # options parsing
    @asRadial = @options.asRadial or true
    @asDiff = @options.asDiff or false
    @animated = @options.animated or false
    @animationOffset = @options.animationOffset or 0
    @valueFormat = @options.valueFormat or '{0}'
    @showData = @options.showData or false
    @dataPointColor = @options.dataPointColor or @theme.clearColor
    @dataPointFunction = @options.dataPointFunction or (dataValue)->
      dataString = dataValue.toString()
      if dataString.length > 3
        dataString = dataString.substr(0, 3);
      return dataString

    # binds
    @animationFrameProxy = @onAnimationFrameTick.bind(@)
    @canvas = @element.querySelector('canvas.chart')
    @valueElement = @element.querySelector('.current-value')

    @ctx = @canvas.getContext('2d')

    @width = @canvas.width
    @height = @canvas.height
    @radius = Math.min(@width, @height)/2
    @startRadius = Math.ceil(@radius / 1.75)
    @maxRadius = @radius - @startRadius
    @center = 
      x: Math.floor(@width/2)
      y: Math.floor(@height/2)
    @ctx.lineWidth = 1.0

    #@highColor = @theme.highColor.replace(/rgb(?:a|)\((\d+),\s*(\d+),\s*(\d+)(,\s*(\d+\.\d+)|)\)/, '$1,$2,$3$4').split(',')
    #@mediumColor = @theme.mediumColor.replace(/rgb(?:a|)\((\d+),\s*(\d+),\s*(\d+)(,\s*(\d+\.\d+)|)\)/, '$1,$2,$3$4').split(',')
    #@lowColor = @theme.lowColor.replace(/rgb(?:a|)\((\d+),\s*(\d+),\s*(\d+)(,\s*(\d+\.\d+)|)\)/, '$1,$2,$3$4').split(',')

    @highColor = @theme.highColor
    @mediumColor = @theme.mediumColor
    @lowColor = @theme.lowColor

    @colors = @colors or [@theme.iconColor, @theme.highlightColor]#@mediumColor,@highColor]
    @currentSpoke = 0
    @currentFrame = 0
    @offsetRan = @animationOffset == 0 or false
    @totalFrames = 45
    @totalBarFrames = @totalFrames + Math.floor(@totalFrames*0.33)
    @totalBarFramesArray = []

    # diff -- call before updateData
    if @asDiff
      @diffDataArray = []

    # initialize
    if not @dataArray
      return
    else
      setData(@dataArray)

  animate: ->
    if @animated and @dataArray != null
      # reset
      @ctx.clearRect(0, 0, @width, @height)
      @offsetRan = @animationOffset == 0 or false
      @currentFrame = 0
      @setData(@dataArray)

  setCurrentValue: (value) ->
    @valueElement.innerHTML = @valueFormat.replace(/\{0\}/g, value);

    if @animated
      @valueElement.classList.add('animated-out')
      document.body.clientWidth;
      @valueElement.classList.remove('animated-out')

  setData: (@dataArray, currentValue=null) ->
    @updateData(@dataArray)

    if @animated
      # build totalFrames for each spoke
      for i in [0..@dataArray.length]
        frameCount = easypeasy.sineOut(i/@dataArray.length)
        @totalBarFramesArray[i] = frameCount
      
      @onAnimationFrameTick(0)

    else
      @currentFrame = @totalFrames
      @currentSpoke = @dataArray.length
      @totalFrames[@dataArray.length-1] = 5

      if not @asRadial
        @setStroke(@width, @height)
        @drawGradient()
      else
        @setRadialStroke(@radius)
        @drawRadialGradient()
    return

  onAnimationFrameTick: (tickTime) ->
    if not @offsetRan
      @animationId = requestAnimationFrame(@animationFrameProxy)
      @currentFrame++
      if @currentFrame > @animationOffset
        @offsetRan = true
        @currentFrame = 0
      else
        return

    if not @asRadial
      @setStroke(@width, @height)
      @drawGradient()
    else
      @setRadialStroke(@radius)
      @drawRadialGradient()

    @currentFrame++

    if @currentFrame <= @totalFrames
      @currentSpoke = Math.ceil(easypeasy.sineOut(@currentFrame/@totalFrames) * @dataArray.length)
    else
      @currentSpoke = @dataArray.length

    if @currentFrame < @totalBarFrames
      @animationId = requestAnimationFrame(@animationFrameProxy)
    return

  draw: ->
    @ctx.clearRect(0, 0, @width, @height)
    for interval in [@width..1]
      if interval < @dataArray.length
        @ctx.strokeStyle = @colorForLevel((@dataArray[interval]/@max), @colors)
        @ctx.beginPath()
        @ctx.moveTo(interval, @height)
        @ctx.lineTo(interval, Math.floor(@height*(@dataArray[interval]/@max)))
        @ctx.closePath()
        @ctx.stroke()

  setStroke: (width, height) ->
    @gradient = @ctx.createLinearGradient(width/2, height, width/2, 0)
    step = 1.0/@colors.length
    for color, i in @colors
      @gradient.addColorStop(step*i, color)
    @ctx.strokeStyle = @gradient

  setRadialStroke: (radius) ->
    @gradient = @ctx.createRadialGradient(@center.x, @center.y, @startRadius, @center.x, @center.y, @radius)
    step = 1.0/@colors.length
    for color, i in @colors
      @gradient.addColorStop(step*i, color)
    @ctx.fillStyle = @gradient
    @ctx.strokeStyle = @gradient

  drawSpoke: (startAngle, angleInterval, radius) ->
    startX = @center.x + (@startRadius * Math.sin(startAngle))
    startY = @center.y + (@startRadius * Math.cos(startAngle))
    endX = startX + (radius * Math.sin(startAngle))
    endY = startY + (radius * Math.cos(startAngle))
    @ctx.lineTo(startX, startY)
    @ctx.lineTo(endX, endY)

    startX = @center.x + (@startRadius * Math.sin(startAngle + angleInterval))
    startY = @center.y + (@startRadius * Math.cos(startAngle + angleInterval))
    endX = startX + (radius * Math.sin(startAngle + angleInterval))
    endY = startY + (radius * Math.cos(startAngle + angleInterval))
    @ctx.lineTo(endX, endY)
    @ctx.lineTo(startX, startY)

  drawRadialGradient: ->
    colorInterval = 1.0/@colors.length
    # for color,i in @colors
    #   min = colorInterval*i
    #   max = min + colorInterval

    @ctx.clearRect(0, 0, @width, @height)
    startAngle = CHART_START_ANGLE
    angleInterval = TAU / @dataArray.length
    # @ctx.moveTo(startX, startY)
    @ctx.moveTo(@center.x, @center.y)
    previousRadius = 0
    previousAngle = startAngle
    @ctx.globalCompositeOperation = 'source-over'
    @setRadialStroke()
    @ctx.beginPath()
    if @asDiff
      dataArray = @diffDataArray
    else
      dataArray = @dataArray
    for dataValue, i in dataArray
      if i > @currentSpoke
        break
      p = @currentFrame/@totalBarFrames
      p = p * @totalBarFramesArray[i] + (@currentFrame/@totalBarFrames)
      if p > 1.0
        p = 1.0
      # p = easypeasy.sineOut(p)

      value = dataValue/@max
      radius = Math.floor(@maxRadius*value)
      # if i == @currentSpoke
      radius = easypeasy.backOutCustom(p, 1.25) * radius
      # radius = easypeasy.circularOut(p) * radius

      @drawSpoke(previousAngle, angleInterval, radius)

      # if the data is too long, shorten it
      currentAngle = startAngle + (i * angleInterval)
      if @showData
        dataString = @dataPointFunction(@dataArray[i]).toString()
        if i == 0
          textStartAngle = previousAngle - angleInterval
        else
          textStartAngle = previousAngle
        @drawTextAtRadian(textStartAngle, angleInterval, 10, dataString, @dataPointColor, @startRadius*-1.02)

      # @ctx.arcTo(previousX, previousY, endX, endY, 4)
      previousRadius = radius
      previousAngle = currentAngle

    # if it's the last spoke the angle is the inverse of startAngle
    if @currentSpoke >= @dataArray.length
      @drawSpoke(startAngle-angleInterval, angleInterval, radius)

    @ctx.moveTo(@center.x+@startRadius, @center.y)
    @ctx.closePath()
    @ctx.globalCompositeOperation = 'destination-over'
    @ctx.fill()
    # @ctx.stroke()
    @ctx.beginPath()
    @ctx.globalCompositeOperation = 'destination-out'
    @ctx.fillStyle = 'white'
    @ctx.arc(@center.x, @center.y, @startRadius, 0, TAU)
    @ctx.closePath()
    @ctx.fill()
    @ctx.globalCompositeOperation = 'source-over'
    # @ctx.moveTo(@center.x + @startRadius, @center.y + @startRadius)
  
  drawTextAtRadian: (startRadian, angleInterval, size, text, color, radius=@startRadius*-1.02) ->
    oldFillStyle = @ctx.fillStyle
    @ctx.fillStyle = color
    @ctx.textAlign = 'center'
    charCount = text.length
    centerOffset = 0
    if (charCount > 1)
      centerOffset = Math.floor(size/2) * (Math.floor(charCount/2))
    else
      centerOffset = Math.floor(size/4)

    # length = radius * radian
    radian = startRadian + (angleInterval/2)
    radian -= ((centerOffset)/radius)
    # center the text based on a guessed textWidth
    # 
    #radian += (radius * (endRadian - startRadian))/2
    # if startRadian == CHART_START_ANGLE
      # radian = startRadian + (endRadian/2)
      # console.log(endRadian)
    sx = centerOffset
    sy = radius
    rotation = (Math.PI*2) - ((CHART_START_ANGLE) + radian)
    
    # cairo_save(cr)
    @ctx.translate(@center.x, @center.y)
    @ctx.rotate(rotation)
    @ctx.moveTo(sx, sy)
    @ctx.fillText(text, sx, sy, size)
    @ctx.moveTo(0, 0)
    @ctx.rotate(-rotation)
    @ctx.translate(-@center.x, -@center.y)
    @ctx.fillStyle = oldFillStyle

  drawGradient: ->
    colorInterval = 1.0/@colors.length
    # for color,i in @colors
    #   min = colorInterval*i
    #   max = min + colorInterval

    @ctx.clearRect(0, 0, @width, @height)
    for interval in [0..@width]
      if interval < @dataArray.length
        x = interval #@width - interval
        value = @dataArray[interval]/@max

        height = Math.floor(@height*value)
        @ctx.beginPath()
        @ctx.moveTo(x, @height)
        @ctx.lineTo(x, height)
        @ctx.closePath()
        @ctx.stroke()

        # lastY = @height
        # for pixel in [0..height]
        #   y = @height-pixel
        #   @ctx.strokeStyle = @gradient #@colorForLevel(1.0-(pixel/height), @colors)
        #   @ctx.beginPath()
        #   @ctx.moveTo(x, lastY)
        #   @ctx.lineTo(x, y)
        #   @ctx.closePath()
        #   @ctx.stroke()
        #   lastY = y

  updateData: () ->
    @max = 0
    @min = 4294967295
    for data in @dataArray
      @max = Math.max(data, @max)
      @min = Math.min(data, @min)

    # to make charts more dramatic, we can change it to a chart of differences
    if @asDiff
      diffMin = @min*0.93
      for data, i in @dataArray
        @diffDataArray[i] = data - diffMin

      @max = @max-diffMin

  colorForLevel: (percentage, colors) ->
    interval = 1.0/colors.length

    for color,i in colors
      min = interval*i
      max = min + interval

      if i+1 < colors.length
        nextColor = colors[i+1]
      else
        nextColor = color
      if percentage > min and percentage <= max
        relativePercentage = (percentage - min)/interval
        rDiff = parseInt(parseInt(nextColor[0] - color[0]))
        gDiff = parseInt(parseInt(nextColor[1] - color[1]))
        bDiff = parseInt(parseInt(nextColor[2] - color[2]))
        r = parseInt(color[0]) + ((rDiff)*relativePercentage)
        g = parseInt(color[1]) + ((gDiff)*relativePercentage)
        b = parseInt(color[2]) + ((bDiff)*relativePercentage)
        a = Number(color[3]) + (Number(color[3]) - Number(nextColor[3]))*relativePercentage

        return "rgba(#{parseInt(r)},#{parseInt(g)},#{parseInt(b)},#{a})"



