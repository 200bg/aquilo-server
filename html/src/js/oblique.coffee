window.oblique = {}

# pretty much a struct?
oblique.themes = {
	"oblivion": {
		background: 'rgba(0,0,0,1.0)'
		glowColor: 'rgba(92,190,209,0.33)'
		faintColor: 'rgba(92,190,209,0.1)'
		nightColor: 'rgba(92,190,209,0.1)'
		dayColor: 'rgba(92,190,209,0.8)'
		baseColor: 'rgba(92,190,209,0.1)'
		iconColor: 'rgba(92,190,209,0.5)'
		highlightColor: 'rgba(92,190,209,0.8)'
		alertColor: 'rgba(255,0,0,0.75)'
		clearColor: 'rgba(0, 0, 0, 0.0)'
	}
}

class oblique.Clock
	FULL_CIRCLE = Math.PI*2
	HALF_CIRCLE = Math.PI
	QUARTER_CIRCLE = Math.PI/2

	HOUR_RADIAN = FULL_CIRCLE/24
	MINUTE_RADIAN = FULL_CIRCLE/(24*60)
	SECOND_RADIAN = FULL_CIRCLE/(24*60*60)
	MILLISECOND_RADIAN = FULL_CIRCLE/(24*60*60*1000)

	SEGMENTS = 14

	MILLISECONDS_IN_A_DAY = 86400000

	# canvas arcs start on the right side, linear radians start at the bottom.
	# subtract this from the radians on an arc command to make them synch up
	CANVAS_ARC_OFFSET_RADIAN = Math.PI/2

	CLOCK_START = Math.PI

	ARC_RADIUS = 180.0
	ARC_THICKNESS = 30
	GLOW_ARC_THICKNESS = 32

	ICON_SIZE = 12
	ICON_PADDING = 2

	#REFRESH_RATE = 2
	REFRESH_RATE = 180
	PULSE_RATE = REFRESH_RATE * 10


	constructor: (@baseCanvas, @clockCanvas, @animationCanvas, @data, @options, @theme) ->
		@animationContext = @animationCanvas.getContext('2d')

		@drawingCanvas = document.createElement('canvas')
		@drawingCanvas.width = @clockCanvas.width
		@drawingCanvas.height = @clockCanvas.height

		@context = @drawingCanvas.getContext('2d')
		@outputContext = @clockCanvas.getContext('2d')
		@centerPoint = @clockCanvas.width/2

		@currentFrame = 59
		@pulseFrame = 0
		#@currentRotationOffset = 0

		@lat = @data.latitude
		@lon = @data.longitude

		#@currentTime = new Date(2013, 3, 29, 6, 41, 0)
		@currentTime = new Date()
		@currentRotationOffset = QUARTER_CIRCLE + @getRadianForTime(@currentTime, @getMidnight(@currentTime))

		tzOffset = @currentTime.getTimezoneOffset()/-60
		if @currentTime.isDST() then tzOffset--
		console.log(tzOffset)

		@sunrise = noaa.solar.calculateSunrise(@currentTime, @lat, @lon, tzOffset, @currentTime.isDST())
		@sunset = noaa.solar.calculateSunset(@currentTime, @lat, @lon, tzOffset, @currentTime.isDST())
		@noon = noaa.solar.calculateSolarNoon(@currentTime, @lon, tzOffset, @currentTime.isDST())

		# DEBUGGING
		# @sunrise = new Date()
		# @sunset = new Date(@sunrise.valueOf() + (4*60*60*1000))
		# @noon = new Date(@sunrise.valueOf() + (2*60*60*1000))

		@sunriseRadian = @getRadianForTime(@sunrise)
		@sunsetRadian = @getRadianForTime(@sunset)

		# console.log(@sunrise)
		# console.log(@sunset)
		# console.log(@noon)

		@animationFrameProxy = @onAnimationFrameTick.bind(this)
		@demoAnimationFrameProxy = @onDemoAnimationFrameTick.bind(this)
		@animationId = 0
		@start()

		@drawBase()
		@draw()

		@hasUpdates = false

	start: ->
		cancelAnimationFrame(@animationId)
		@animationId = requestAnimationFrame(@animationFrameProxy)

	stop: ->
		cancelAnimationFrame(@animationId)

	startDemo: ->
		cancelAnimationFrame(@animationId)
		now = new Date()
		@currentTime = new Date(now.getYear(), now.getMonth(), now.getDate(), 0, 0, 0)
		console.log(@currentTime)
		@animationId = requestAnimationFrame(@demoAnimationFrameProxy)

	getMidnight: (time) ->
		midnight = new Date(time.valueOf())
		return midnight.setHours(0,0,0,0)

	setTime: (@currentTime) ->
		#@currentTime = new Date(2013, 3, 29, 6, 41, 0)
		#console.log(@currentTime)
		#msSinceMidnightEpoch = @currentTime.valueOf() - @midnight.valueOf();
		#console.log(sSinceMidnightEpoch)
		#@currentRotationOffset = (msSinceMidnightEpoch * MILLISECOND_RADIAN) - CLOCK_START
		#@currentRotationOffset = CLOCK_START - @getRadianForTime(@currentTime)

		#just rotate the whole context
		#console.log(@currentTime)
		@currentRotationOffset = QUARTER_CIRCLE + @getRadianForTime(@currentTime, @getMidnight(@currentTime))
		@outputContext.clearRect(0,0,@clockCanvas.width,@clockCanvas.height)
		@outputContext.save()
		#@context.rotate(@getRadianForTime(@currentTime))
		@outputContext.translate(@centerPoint, @centerPoint)
		# rotation is counterclockwise
		@outputContext.rotate(-@currentRotationOffset)
		@outputContext.translate(-@centerPoint, -@centerPoint)
		# @draw()
		@outputContext.drawImage(@drawingCanvas, 0, 0)
		@currentFrame = 0
		@outputContext.restore()

		#draw all hours
		for hour in [1..24]
			if hour % 6 == 0
				if hour % 12 == 0
					@drawTickAtRadian(@outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + @currentRotationOffset, 10, @theme.baseColor)
				else
					@drawTickAtRadian(@outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + @currentRotationOffset, 6, @theme.baseColor)
			else
				@drawTickAtRadian(@outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + @currentRotationOffset, 2, @theme.baseColor)

			if hour%12 == 0 then formattedHour = 12 else formattedHour = hour%12
			@drawTextAtRadian(@outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + @currentRotationOffset, 20, formattedHour, @theme.iconColor)

	

	getRadianForTime: (dateTime, relativeTo=@currentTime) ->
		duration = dateTime - relativeTo
		percentOfArc = duration/MILLISECONDS_IN_A_DAY
		#angleForTime = (dateTime.valueOf() - @midnight.valueOf()) * -MILLISECOND_RADIAN
		#angleForTime = (dateTime.getHours() * - HOUR_RADIAN) + (dateTime.getMinutes() * -MINUTE_RADIAN) + (dateTime.getSeconds() * -SECOND_RADIAN)
		angleForTime = (percentOfArc * FULL_CIRCLE)
		# angleForTime = (percentOfArc * FULL_CIRCLE)
		return angleForTime

	clear: ->
		@context.clearRect(0,0,@clockCanvas.width,@clockCanvas.height)

	updateClock: ->
		@draw()
		return

	# should draw the initial graphics and have very few updates afterwards.
	drawBase: ->
		context = @baseCanvas.getContext('2d')
		context.clearRect(0,0,@baseCanvas.width,@baseCanvas.height)
		#draw the whole arc
		context.strokeStyle = @theme.baseColor
		context.lineWidth = ARC_THICKNESS
		context.beginPath()
		context.arc(@centerPoint, @centerPoint, ARC_RADIUS, 0, FULL_CIRCLE, false);
		context.closePath()
		context.stroke();

		#draw the line for person
		context.strokeStyle = @theme.iconColor
		context.lineWidth = 1
		context.beginPath()
		context.moveTo(@centerPoint, @centerPoint-ARC_RADIUS-ARC_THICKNESS-(ICON_SIZE+ICON_PADDING))
		context.lineTo(@centerPoint, @centerPoint-ARC_RADIUS-ARC_THICKNESS-ICON_PADDING)
		context.closePath()
		context.stroke()

	draw: ->
		@context.clearRect(0,0,@clockCanvas.width,@clockCanvas.height)
		#draw the sun arc
		###
		@context.strokeStyle = @theme.highlightColor
		@context.lineWidth = GLOW_ARC_THICKNESS
		@context.beginPath()
		sSr = @currentRotationOffset+@sunriseRadian
		sSs = @currentRotationOffset+@sunsetRadian
		ssDiff = @sunsetRadian - @sunriseRadian
		ex = @centerPoint + ARC_RADIUS * Math.sin(sSs)
		ey = @centerPoint + ARC_RADIUS * Math.cos(sSs)
		@context.arc(@centerPoint, @centerPoint, ARC_RADIUS, CANVAS_ARC_OFFSET_RADIAN - sSr, ssDiff, false);
		# this keeps it from closing the arc
		@context.moveTo(ex, ey);
		@context.closePath()
		@context.stroke();
		###
		# DEBUGGING where it should start and end

		# @drawTimeSpanArc(@sunrise, @sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.highlightColor, @theme.glowColor)
		@drawSunPathArcs(@sunrise, @sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.dayColor)
		diff = Math.abs(@sunrise - @sunset)
		@drawSunPathArcs(@sunset, @sunset.valueOf() + diff, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.nightColor)

		#draw midnight		
		#@context.strokeStyle = @theme.alertColor
		#@context.lineWidth = 1
		#@context.beginPath()
		#sx = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(ICON_SIZE+ICON_PADDING)) * Math.sin(@currentRotationOffset+QUARTER_CIRCLE)
		#sy = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(ICON_SIZE+ICON_PADDING)) * Math.cos(@currentRotationOffset+QUARTER_CIRCLE)
		#ex = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.sin(@currentRotationOffset+QUARTER_CIRCLE)
		#ey = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.cos(@currentRotationOffset+QUARTER_CIRCLE)

		#@context.moveTo(sx, sy)
		#@context.lineTo(ex, ey)
		#@context.closePath()
		#@context.stroke()

		#draw current time
		#@context.strokeStyle = @theme.alertColor
		#@context.lineWidth = 1
		#@context.beginPath()
		#angleForNow = QUARTER_CIRCLE - @getRadianForTime(@currentTime)
		#sx = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(40+ICON_PADDING)) * Math.sin(angleForNow)
		#sy = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(40+ICON_PADDING)) * Math.cos(angleForNow)
		#ex = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.sin(angleForNow)
		#ey = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.cos(angleForNow)

		#@context.moveTo(sx, sy)
		#@context.lineTo(ex, ey)
		#@context.closePath()
		#@context.stroke()

		#draw the sun

		sx = null
		sy = null
		ex = null
		ey = null
		sSr = null
		sSs = null
		ssDiff = null

		return

	animate: ->
		@animationContext.clearRect(0, 0, @animationCanvas.width, @animationCanvas.height);
		# draw the center animation phase
		factor = easing.easeOutQuad(@pulseFrame, @pulseFrame, 0.0, 1.0, PULSE_RATE)
		centerRadius = 164+(30*factor)
		if @animationContext.setStrokeColor
			@animationContext.setStrokeColor(@theme.faintColor, easing.easeOutYoYo(@pulseFrame, @pulseFrame, 0.0, 1.0, PULSE_RATE))
		else
			@animationContext.strokeStyle = @theme.faintColor
		#@animationContext.strokeStyle = @theme.faintColor
		@animationContext.shadowColor = @theme.baseColor;
		@animationContext.shadowBlur = 20;
		@animationContext.shadowOffsetX = 0;
		@animationContext.shadowOffsetY = 0;
		@animationContext.lineWidth = 12 - (12*factor)
		@animationContext.beginPath()
		@animationContext.arc(@centerPoint, @centerPoint, centerRadius, 0, FULL_CIRCLE)
		@animationContext.closePath()
		@animationContext.stroke()
		@animationContext.fill()

	drawTickAtRadian: (context, radian, size, color) ->
		context.strokeStyle = color
		context.lineWidth = 1
		context.beginPath()
		sx = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING)) * Math.sin(radian)
		sy = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING)) * Math.cos(radian)
		ex = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.sin(radian)
		ey = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.cos(radian)

		context.moveTo(sx, sy)
		context.lineTo(ex, ey)
		context.closePath()
		context.stroke()

	drawTextAtRadian: (context, radian, size, text, color) ->
		sx = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING + ICON_PADDING)) * Math.sin(radian)
		sy = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING + ICON_PADDING)) * Math.cos(radian)

		angle = Math.atan2(@centerPoint-sy, @centerPoint-sx)

		# context.save()
		context.fillStyle = color
		# context.moveTo(sx, sy)
		# context.translate(sx, sy)
		# context.rotate(0)
		context.textAlign = 'center'
		context.fillText(text, sx, sy, 25)
		# context.translate(sx, sy)
		# context.restore()

	drawTimeSpanArc: (startDate, endDate, radius, size, color, shadowColor) ->
		@context.strokeStyle = color
		@context.lineWidth = size
		@context.shadowColor = shadowColor;
		@context.shadowBlur = size;
		@context.shadowOffsetX = 0;
		@context.shadowOffsetY = 0;
		@context.beginPath()

		startDateRadian = @getRadianForTime(startDate, @getMidnight(@currentTime))
		endDateRadian = @getRadianForTime(endDate, @getMidnight(@currentTime))

		sx = @centerPoint + Math.sin(QUARTER_CIRCLE - startDateRadian) * radius
		sy = @centerPoint + Math.cos(QUARTER_CIRCLE - startDateRadian) * radius
		ex = @centerPoint + Math.sin(QUARTER_CIRCLE - endDateRadian) * radius
		ey = @centerPoint + Math.cos(QUARTER_CIRCLE - endDateRadian) * radius

		startAngle = Math.atan2(@centerPoint-sy,@centerPoint-sx);
		endAngle = Math.atan2(@centerPoint-ey,@centerPoint-ex);
		#console.log(@currentRotationOffset - startDateRadian);
		@context.arc(@centerPoint, @centerPoint, radius, startAngle - HALF_CIRCLE, endAngle - HALF_CIRCLE, false);
		# this keeps it from closing the arc
		@context.moveTo(ex, ey)
		@context.closePath()
		@context.stroke()


		# DEBUGGING
		# @context.beginPath()	
		# @context.fillStyle = '#00ff00'
		# @context.arc(sx, sy, 4, 0, FULL_CIRCLE, false);
		# @context.closePath()
		# @context.fill();

		# @context.beginPath()
		# @context.fillStyle = '#ff0000'
		# @context.arc(ex, ey, 4, 0, FULL_CIRCLE, false);
		# @context.closePath()
		# @context.fill();

	# splits up the times
	drawSunPathArcs: (startDate, endDate, radius, size, arcColor) ->
		span = endDate - startDate
		interval = Math.floor(span/SEGMENTS)
		baseColor = arcColor.replace(/[rgba]+?\((.+)\)/, '$1').split(',')
		halfSegments = Math.floor(SEGMENTS/2)
		opacityInterval = baseColor[3]/halfSegments

		for i in [0..SEGMENTS-1]
			if i < halfSegments
				color = "rgba(#{baseColor[0]},#{baseColor[1]},#{baseColor[2]},#{opacityInterval*(i+1)})"
			else
				color = "rgba(#{baseColor[0]},#{baseColor[1]},#{baseColor[2]},#{opacityInterval*(halfSegments - ((i+1)-halfSegments))})"

			@drawTimeSpanArc(startDate.valueOf()+(interval*i), startDate.valueOf()+(interval*(i+1)), radius, size, color, @theme.clearColor)

	onAnimationFrameTick: (tickTime) ->
		@pulseFrame++
		if @pulseFrame >= PULSE_RATE then @pulseFrame = 0
		@currentFrame++
		if @currentFrame >= REFRESH_RATE 
			@setTime(new Date())
		# @animate()

		@animationId = requestAnimationFrame(@animationFrameProxy)

	onDemoAnimationFrameTick: (tick) ->
		nextMinute = new Date(@currentTime.valueOf() + (60*1000))
		@setTime(nextMinute)
		@animationId = requestAnimationFrame(@demoAnimationFrameProxy)
		


	
