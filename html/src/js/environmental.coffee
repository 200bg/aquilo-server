
class ccmm.ThermostatChart extends ccmm.Chart
  constructor: (@element, @dataArray, @theme, @colors, @options={}) ->
    super
    @targetValueElement = @element.querySelector('.target-value')
    @runningTimeElement = @element.querySelector('.running-time')

  setCurrentValue: (temp, targetTemp, runningTime) ->
    if @targetValueElement
      @targetValueElement.innerHTML = targetTemp + '&deg;'

    if @runningTimeElement
      @runningTimeElement.innerHTML = runningTime
      
    if @animated
      @targetValueElement.classList.add('animated-out')
      @runningTimeElement.classList.add('animated-out')
      document.body.clientWidth;
      @targetValueElement.classList.remove('animated-out')
      @runningTimeElement.classList.remove('animated-out')

    super(temp)

  animate: ->
    if @animated
      @targetValueElement.classList.add('animated-out')
      @runningTimeElement.classList.add('animated-out')
      document.body.clientWidth;
      @targetValueElement.classList.remove('animated-out')
      @runningTimeElement.classList.remove('animated-out')

    super

class ccmm.WeatherChart extends ccmm.Chart
  constructor: (@element, @dataArray, @theme, @colors, @options={}) ->
    super
    @humidityElement = @element.querySelector('.humidity')
    @conditionsElement = @element.querySelector('.conditions')

  setCurrentValue: (temp, humidity, conditions) ->
    if @humidityElement
      @humidityElement.innerHTML = humidity + '% humidity'

    if @conditionsElement
      @conditionsElement.innerHTML = conditions

    if @animated
      @humidityElement.classList.add('animated-out')
      @conditionsElement.classList.add('animated-out')
      document.body.clientWidth
      @humidityElement.classList.remove('animated-out')
      @conditionsElement.classList.remove('animated-out')

    super(temp)

  animate: () ->
    if @animated
      @humidityElement.classList.add('animated-out')
      @conditionsElement.classList.add('animated-out')
      document.body.clientWidth
      @humidityElement.classList.remove('animated-out')
      @conditionsElement.classList.remove('animated-out')

    super

class ccmm.PollenChart extends ccmm.Chart
  constructor: (@element, @dataArray, @theme, @colors, @options={}) ->
    super
    @pollenTypeElement = @element.querySelector('.pollen-type')

  setCurrentValue: (count, type) ->
    if @pollenTypeElement
      @pollenTypeElement.innerHTML = type

    if @animated
      @pollenTypeElement.classList.add('animated-out')
      document.body.clientWidth
      @pollenTypeElement.classList.remove('animated-out')
    super(count)

  animate: ->
    if @animated
      @pollenTypeElement.classList.add('animated-out')
      document.body.clientWidth
      @pollenTypeElement.classList.remove('animated-out')

    super


class ccmm.EnvironmentalControls extends ccmm.Section
  WEATHER_API = 'http://api.openweathermap.org/data/2.5/weather?q={0}'
  # not sure how long this will last... : gets forecast too
  POLLEN_API = 'http://www.claritin.com/weatherpollenservice/weatherpollenservice.svc/getforecast/{0}' #zipcode
  WEATHER_TEST = {"coord":{"lon":-95.37,"lat":29.76},"sys":{"message":0.051,"country":"United States of America","sunrise":1399289694,"sunset":1399338080},"weather":[{"id":800,"main":"Clear","description":"Sky is Clear","icon":"01n"}],"base":"cmc stations","main":{"temp":290.91,"pressure":1014,"temp_min":288.15,"temp_max":293.71,"humidity":91},"wind":{"speed":3.22,"deg":180.503},"clouds":{"all":0},"dt":1399269629,"id":4699066,"name":"Houston","cod":200}

  constructor: (@element, @location, @theme) ->
    @weatherUrl = WEATHER_API.replace('{0}', @location.name)
    @weatherData = WEATHER_TEST

    ambientOptions =
      asRadial: true
      asDiff: true
      animated: true
      animationOffset: 0
      valueFormat: '{0}&deg;'
      showData: true
      dataPointColor: @theme.background
      dataPointFunction: Math.floor
    @ambientChart = new ccmm.ThermostatChart(document.getElementById('ambient-chart'), null, @theme, null, ambientOptions)

    weatherOptions =
      asRadial: true
      asDiff: true
      animated: true
      animationOffset: 15
      valueFormat: '{0}&deg;'
      showData: true
      dataPointColor: @theme.background
      dataPointFunction: (d)-> return Math.floor(d) + '°'
    @weatherChart = new ccmm.WeatherChart(document.getElementById('weather-chart'), null, @theme, [@theme.alertColor, @theme.highColor], weatherOptions)

    pollenOptions =
      asRadial: true
      animated: true
      showData: true
      dataPointColor: @theme.background
      animationOffset: 30
    @pollenChart = new ccmm.PollenChart(document.getElementById('pollen-chart'), null, @theme, [@theme.dateDayColor, @theme.mediumColor], pollenOptions)
    # @getWeather()
    return

  activate: ->
    super
    @ambientChart.animate()
    @weatherChart.animate()
    @pollenChart.animate()

  getWeather: ->
    ajax(@weatherUrl, @onWeatherReceivedProxy, 'GET', null, 'application/json')

  displayWeather: ->
    @weatherData
    return

  onWeatherReceived: (error, text, xhr) ->
    @weatherData = JSON.parse(text)

