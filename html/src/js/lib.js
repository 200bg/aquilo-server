// requestAnimationFrame polyfill
(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());


if (!window.devicePixelRatio)
    window.devicePixelRatio = 1.0

(function() {
    window.ajax = function(url, callback, method, postData, contenttype) {
        var requestTimeout,xhr;
        try { 
            xhr = new XMLHttpRequest(); 
        }catch(e){
            try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e){
                if(console)console.log("AJAX Error: XMLHttpRequest not supported");
                return null;
            }
        }

        requestTimeout = setTimeout(function() {xhr.abort(); callback(new Error("AJAX Error: aborted by a timeout"), "",xhr); }, 5000);
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            clearTimeout(requestTimeout);
            callback(xhr.status != 200 ? new Error("AJAX Error: server respnse status is "+xhr.status):false, xhr.responseText, xhr);
        }
        xhr.open(method?method.toUpperCase():"GET", url, true);
        if(!postData)
            xhr.send();
        else {
            xhr.setRequestHeader('Content-type', contenttype?contenttype:'application/x-www-form-urlencoded');
            xhr.send(postData)
        }
    }

    window.ajaxSync = function(url, callback, method, postData, contenttype) {
        var xhr;
        try {
            xhr = new XMLHttpRequest();
        } catch(e){
            try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }catch (e){
                if(console)console.log("AJAX Error: XMLHttpRequest not supported");
                return null;
            }
        }

        xhr.open(method?method.toUpperCase():"GET", url, false);
        if(!postData)
            xhr.send();
        else {
            xhr.setRequestHeader('Content-type', contenttype?contenttype:'application/x-www-form-urlencoded');
            xhr.send(postData)
        }
        if (xhr.readyState != 4) return;
        callback(xhr.status != 200?new Error("AJAX Error: server respnse status is "+xhr.status):false, xhr.responseText,xhr);
    }
})());