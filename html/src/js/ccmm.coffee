window.ccmm = window.ccmm || {}

#  these are constants, ignore them

FULL_CIRCLE = Math.PI*2
HALF_CIRCLE = Math.PI
QUARTER_CIRCLE = Math.PI/2

HOUR_RADIAN = FULL_CIRCLE/24
MINUTE_RADIAN = FULL_CIRCLE/(24*60)
SECOND_RADIAN = FULL_CIRCLE/(24*60*60)
MILLISECOND_RADIAN = FULL_CIRCLE/(24*60*60*1000)

SEGMENTS = 12

# MILLISECONDS_IN_A_DAY = 86400
MILLISECONDS_IN_A_DAY = 86400000
#  MILLISECONDS_IN_A_YEAR = 31557600
#  MILLISECONDS_IN_A_MONTH = 31557600

#  canvas arcs start on the right side, linear radians start at the bottom.
#  subtract this from the radians on an arc command to make them synch up
CANVAS_ARC_OFFSET_RADIAN = Math.PI/2

CLOCK_START = Math.PI

currentRotationOffset = 0
currentTime = new Date()

alphaRegex = /,([\d\.]+)\)$/m
extractAlpha = (color)->
  # return NaN
  if color.indexOf('rgba') == 0
    return Number(color.match(alphaRegex)[1])
  return 1.0

prettyHour = (time) ->
  hour = time.getHours()
  if hour > 12
    hour = hour - 12
  if hour == 0
    hour = 12
  if hour < 10
    hour = '0' + hour

  return hour

prettyMinute = (time) ->
  minute =  time.getMinutes()
  if minute < 10
    minute = '0' + minute

  return minute

prettyAMPM = (time) ->
  hour = time.getHours()
  if hour >= 12
    return 'PM'
  return 'AM'

ICON_SIZE = 12
ICON_PADDING = -5
TEXT_ICON_PADDING = -4
CLOCK_WIDTH = 390
CLOCK_HEIGHT = 390
CLOCK_RADIUS = Math.floor(CLOCK_WIDTH/2)
OFFSET_X = 127
OFFSET_Y = 70
ARC_RADIUS = CLOCK_RADIUS
ARC_THICKNESS = 40
GLOW_ARC_THICKNESS = 40
TICK_SIZE = 12
LATITUDE = 29.681416
LONGITUDE = -95.383172
REFRESH_RATE = 60
HOUR_TO_RESET = 20 # in hours

MONTHS = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

class ccmm.Tabset
  constructor: (@element, @sections) ->
    @tabs = @element.querySelectorAll('.tab')
    @onClickProxy = @onClick.bind(@)

    for tab in @tabs
      tab.addEventListener('click', @onClickProxy)

  activate: (name) ->
    for tab in @tabs
      tabName = tab.getAttribute('data-name')
      if tabName == name
        @sections[name].activate()
        tab.classList.add('active')
      else
        @sections[tabName].deactivate()
        tab.classList.remove('active')

  onClick: (e) ->
    tab = e.currentTarget
    tabName = tab.getAttribute('data-name')
    @activate(tabName)


class ccmm.Section
  constructor: (@element) ->
    @isActive = false

  activate: ->
    @isActive = true
    @element.style.display = null

  deactivate: ->
    @isActive = false
    @element.style.display = 'none'

class ccmm.Clock extends ccmm.Section
  
  constructor: (@clockElement, @baseCanvas, @clockCanvas, @animationCanvas, @timeElement, @dateElement, @data, @options={}, @theme=ccmm.themes['tech-49']) ->
    super(@clockElement)
    @plugins = []
    ICON_SIZE = @options.ICON_SIZE or ICON_SIZE
    ICON_PADDING = @options.ICON_PADDING or ICON_PADDING
    TEXT_ICON_PADDING = @options.TEXT_ICON_PADDING or TEXT_ICON_PADDING
    CLOCK_WIDTH = @options.CLOCK_WIDTH or CLOCK_WIDTH
    CLOCK_HEIGHT = @options.CLOCK_HEIGHT or CLOCK_HEIGHT
    CLOCK_RADIUS = @options.CLOCK_RADIUS or CLOCK_RADIUS
    OFFSET_X = @options.OFFSET_X or OFFSET_X
    OFFSET_Y = @options.OFFSET_Y or OFFSET_Y
    ARC_RADIUS = @options.ARC_RADIUS or ARC_RADIUS
    ARC_THICKNESS = @options.ARC_THICKNESS or ARC_THICKNESS
    GLOW_ARC_THICKNESS = @options.GLOW_ARC_THICKNESS or GLOW_ARC_THICKNESS
    TICK_SIZE = @options.TICK_SIZE or TICK_SIZE
    LATITUDE = @options.LATITUDE or LATITUDE
    LONGITUDE = @options.LONGITUDE or LONGITUDE
    HOUR_TO_RESET = @options.HOUR_TO_RESET or HOUR_TO_RESET

    @animationContext = @animationCanvas.getContext('2d')

    @drawingCanvas = document.createElement('canvas')
    @drawingCanvas.width = @clockCanvas.width
    @drawingCanvas.height = @clockCanvas.height

    @context = @drawingCanvas.getContext('2d')
    @outputContext = @clockCanvas.getContext('2d')
    @centerPoint = @clockCanvas.width/2

    @currentFrame = 59
    @pulseFrame = 0
    #@currentRotationOffset = 0

    @lat = @data.latitude
    @lon = @data.longitude

    #@currentTime = new Date(2013, 3, 29, 6, 41, 0)
    @currentTime = new Date()
    @currentDate = @currentTime.getDate()
    @currentRotationOffset = QUARTER_CIRCLE + @getRadianForTime(@currentTime, @getMidnight(@currentTime))

    @monthElement = @dateElement.querySelector('.month')
    @dayElement = @dateElement.querySelector('.day')

    @timeElement.style.color = @theme.dateTimeColor
    @monthElement.style.color = @theme.dateMonthColor
    @dayElement.style.color = @theme.dateDayColor

    window.addEventListener('resize', @onResize.bind(@))

    @resetSolars()

    # DEBUGGING
    # @sunrise = new Date()
    # @sunset = new Date(@sunrise.valueOf() + (4*60*60*1000))
    # @noon = new Date(@sunrise.valueOf() + (2*60*60*1000))

    @sunriseRadian = @getRadianForTime(@sunrise)
    @sunsetRadian = @getRadianForTime(@sunset)

    # console.log(@sunrise)
    # console.log(@sunset)
    # console.log(@noon)

    @animationFrameProxy = @onAnimationFrameTick.bind(this)
    @animationId = 0
    @start()

    @drawBase(@context, 0)
    @setTime(@currentTime)

    @needsReset = false
    @hasUpdates = false

  start: ->
    cancelAnimationFrame(@animationId)
    @animationId = requestAnimationFrame(@animationFrameProxy)

  stop: ->
    cancelAnimationFrame(@animationId)

  activate: ->
    super()
    @clockElement.classList.add('animated-out')
    #forced DOM draw
    document.body.clientWidth
    @clockElement.classList.remove('animated-out')
    @start()

  deactivate: ->
    super()
    @stop()

  resetSolars: ->
    tzOffset = @currentTime.getTimezoneOffset()/-60
    if @currentTime.isDST()
      tzOffset -= 1
    @sunrise = noaa.solar.calculateSunrise(@currentTime, @lat, @lon, tzOffset, @currentTime.isDST())
    @sunset = noaa.solar.calculateSunset(@currentTime, @lat, @lon, tzOffset, @currentTime.isDST())
    @noon = noaa.solar.calculateSolarNoon(@currentTime, @lon, tzOffset, @currentTime.isDST())

  getMidnight: (time) ->
    midnight = new Date(time.valueOf())
    return midnight.setHours(0,0,0,0)

  setTime: (@currentTime) ->
    @currentRotationOffset = QUARTER_CIRCLE + @getRadianForTime(@currentTime, @getMidnight(@currentTime))
    @outputContext.clearRect(0, 0, @clockCanvas.width, @clockCanvas.height)
    # @drawBase(@context, @currentRotationOffset)
    @redraw(-@currentRotationOffset)

    for plugin in @plugins
      try
        plugin.draw(@currentTime)
      catch err
        # console.log(err)
        return
    # offloaded drawing
    # @outputContext.drawImage(@drawingCanvas, 0, 0)
    
    # resetting the refresh rate here:
    @currentFrame = 0

    hour = prettyHour(@currentTime)
    minute = prettyMinute(@currentTime)
    @timeElement.innerHTML = hour + ':' + minute

    date = @currentTime.getDate()
    if date < 10
      date = '0' + date
    @monthElement.innerHTML = MONTHS[@currentTime.getMonth()]
    @dayElement.innerHTML = date

    


  getRadianForTime: (dateTime, relativeTo=@currentTime) ->
    duration = dateTime - relativeTo
    percentOfArc = duration/MILLISECONDS_IN_A_DAY
    #angleForTime = (dateTime.valueOf() - @midnight.valueOf()) * -MILLISECOND_RADIAN
    #angleForTime = (dateTime.getHours() * - HOUR_RADIAN) + (dateTime.getMinutes() * -MINUTE_RADIAN) + (dateTime.getSeconds() * -SECOND_RADIAN)
    angleForTime = (percentOfArc * FULL_CIRCLE)
    # angleForTime = (percentOfArc * FULL_CIRCLE)
    return angleForTime

  clear: ->
    @context.clearRect(0,0,@clockCanvas.width,@clockCanvas.height)

  redraw: (offset) ->
    # just redraws the current day arcs with a rotation
    # expects that the day has already been drawn (should be handled by the tick)
    @outputContext.save()
    @outputContext.translate(@clockCanvas.width/2,@clockCanvas.height/2)
    @outputContext.rotate(offset)
    @outputContext.translate(@clockCanvas.width/-2,@clockCanvas.height/-2)
    @outputContext.drawImage(@drawingCanvas, 0, 0)
    @outputContext.restore()


    #draw the line for marker
    @outputContext.strokeStyle = @theme.iconColor
    @outputContext.lineWidth = 1
    @outputContext.beginPath()
    @outputContext.moveTo(@centerPoint, @centerPoint-ARC_RADIUS-ARC_THICKNESS-(ICON_SIZE+ICON_PADDING))
    @outputContext.lineTo(@centerPoint, @centerPoint-ARC_RADIUS-ARC_THICKNESS-ICON_PADDING)
    @outputContext.closePath()
    @outputContext.stroke()


  # should draw the initial graphics and have very few updates afterwards.
  drawBase: (context, rotationOffset=0) ->
    context.clearRect(0,0,@clockCanvas.width, @clockCanvas.height)

    # cairo_set_operator(cr, CAIRO_OPERATOR_OVER)
    # 
    # drawSegmentArcs(sunrise, sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, theme.dayColor)
    context.globalCompositeOperation = 'source-over'
    # the actual date
    @drawTimeSpanArc(@sunrise.valueOf(), @sunset.valueOf(), ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.dayArcColor, @theme.dayArcColor)
    @drawSegmentArcs(@sunrise, @sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.glowColor, extractAlpha(@theme.glowColor), false, 64, rotationOffset)

    diff = MILLISECONDS_IN_A_DAY-@sunset.valueOf() + (@sunrise.valueOf())
    @drawTimeSpanArc(@sunset.valueOf(), @sunset + diff.valueOf(), ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.nightArcColor, @theme.nightArcColor)
    @drawSegmentArcs(@sunset, @sunset + diff, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.nightColor, true, SEGMENTS, rotationOffset)
    context.globalCompositeOperation = 'screen'
    segmentStart = @getMidnight(@currentTime) + (MILLISECONDS_IN_A_DAY/4) + (MILLISECONDS_IN_A_DAY/8)
    @drawSegmentArcs(segmentStart, segmentStart+(MILLISECONDS_IN_A_DAY/2), ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.dayColor, 0.33, false, SEGMENTS, rotationOffset)
    context.globalCompositeOperation = 'source-over'
    @drawSegmentArcs(segmentStart+(MILLISECONDS_IN_A_DAY/2), segmentStart+MILLISECONDS_IN_A_DAY-1, ARC_RADIUS, GLOW_ARC_THICKNESS, @theme.nightColor, extractAlpha(@theme.nightColor), true, SEGMENTS, rotationOffset)

    #draw all hours
    for hour in [1..24]
      if hour % 6 == 0
        if hour % 12 == 0
          @drawTickAtRadian(@context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 10, @theme.baseColor)
        else
          @drawTickAtRadian(@context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 6, @theme.baseColor)
      else
        @drawTickAtRadian(@context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 2, @theme.baseColor)

      if hour%12 == 0 then formattedHour = 12 else formattedHour = hour%12
      @drawTextAtRadian(@context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 20, formattedHour, @theme.glowColor, rotationOffset)

  animate: ->
    @animationContext.clearRect(0, 0, @animationCanvas.width, @animationCanvas.height);

  drawSegmentArcs: (startDate, endDate, radius, size, arcColor, colorAlpha, invertGradient, segments=SEGMENTS, rotationOffset) ->
    span = endDate.valueOf() - startDate.valueOf()
    interval = Math.floor(span/segments)
    # baseColor = arcColor.replace(/[rgba]+?\((.+)\)/, '$1').split(',')
    halfSegments = Math.floor(segments/2)
    opacityInterval = colorAlpha/halfSegments
    alpha = 1.0

    for i in [0..segments-1]
      if (i < halfSegments)
        alpha = opacityInterval * (i+1.0)
      else
        alpha = opacityInterval * (halfSegments - ((i+1.0)-halfSegments))

      @context.globalAlpha = alpha
      @drawTimeSpanArc(startDate.valueOf()+(interval*i), startDate.valueOf()+(interval*(i+1)), radius, size, arcColor, rotationOffset)
      @context.globalAlpha = 1.0
    
  drawTickAtRadian: (context, radian, size, color) ->
    context.strokeStyle = color
    context.lineWidth = 1
    context.beginPath()
    sx = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING)) * Math.sin(radian)
    sy = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+(size+ICON_PADDING)) * Math.cos(radian)
    ex = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.sin(radian)
    ey = @centerPoint + (ARC_RADIUS+ARC_THICKNESS+ICON_PADDING) * Math.cos(radian)

    context.moveTo(sx, sy)
    context.lineTo(ex, ey)
    context.closePath()
    context.stroke()

  drawTextAtRadian: (context, radian, size, text, color, rotationOffset) ->
    context.fillStyle = color
    context.textAlign = 'center'
    charCount = text.length
    centerOffset = 0
    if (charCount > 1)
      centerOffset = Math.floor(size/2) * (Math.floor(charCount/2))
    else
      centerOffset = Math.floor(size/4)

    sx = centerOffset
    sy = (ARC_RADIUS + ARC_THICKNESS + TEXT_ICON_PADDING)*-1
    rotation = (Math.PI*2) - (rotationOffset + radian)
    
    # cairo_save(cr)
    context.translate(@centerPoint, @centerPoint)
    context.rotate(rotation)
    context.moveTo(sx, sy)
    context.fillText(text, sx, sy, size)
    context.moveTo(0, 0)
    context.rotate(-rotation)
    context.translate(-@centerPoint, -@centerPoint)
    # cairo_restore(cr)

  drawTimeSpanArc: (startDate, endDate, radius, size, color, rotationOffset) ->
    @context.strokeStyle = color
    @context.lineWidth = size
    @context.beginPath()

    startDateRadian = @getRadianForTime(startDate, @getMidnight(@currentTime))
    endDateRadian = @getRadianForTime(endDate, @getMidnight(@currentTime))

    sx = @centerPoint + Math.sin(rotationOffset + QUARTER_CIRCLE - startDateRadian) * radius
    sy = @centerPoint + Math.cos(rotationOffset + QUARTER_CIRCLE - startDateRadian) * radius
    ex = @centerPoint + Math.sin(rotationOffset + QUARTER_CIRCLE - endDateRadian) * radius
    ey = @centerPoint + Math.cos(rotationOffset + QUARTER_CIRCLE - endDateRadian) * radius

    startAngle = Math.atan2(@centerPoint-sy,@centerPoint-sx);
    endAngle = Math.atan2(@centerPoint-ey,@centerPoint-ex);
    #console.log(rotationOffset - startDateRadian);
    @context.arc(@centerPoint, @centerPoint, radius, startAngle - HALF_CIRCLE, endAngle - HALF_CIRCLE, false);
    # this keeps it from closing the arc
    @context.moveTo(ex, ey)
    @context.closePath()
    @context.stroke()



  # splits up the times
  drawSunPathArcs: (startDate, endDate, radius, size, arcColor, rotationOffset=0) ->
    span = endDate - startDate
    interval = Math.floor(span/SEGMENTS)
    baseColor = arcColor.replace(/[rgba]+?\((.+)\)/, '$1').split(',')
    halfSegments = Math.floor(SEGMENTS/2)
    opacityInterval = baseColor[3]/halfSegments

    for i in [0..SEGMENTS-1]
      if i < halfSegments
        color = "rgba(#{baseColor[0]},#{baseColor[1]},#{baseColor[2]},#{opacityInterval*(i+1)})"
      else
        color = "rgba(#{baseColor[0]},#{baseColor[1]},#{baseColor[2]},#{opacityInterval*(halfSegments - ((i+1)-halfSegments))})"

      @drawTimeSpanArc(startDate.valueOf()+(interval*i), startDate.valueOf()+(interval*(i+1)), radius, size, color, @theme.clearColor)

  onAnimationFrameTick: (tickTime) ->

    if (@currentTime.getHours() == HOUR_TO_RESET and @needsReset) or @currentTime.getDate() != @currentDate
      @needsReset = false
      @resetSolars()
      @drawBase(@context, 0)
      @currentDate = @currentTime.getDate()
    else if @currentTime.getHours() != HOUR_TO_RESET
      @needsReset = true

    @currentFrame++
    if @currentFrame >= REFRESH_RATE 
      # @setTime(new Date(@currentTime.valueOf() + 3600000))
      @setTime(new Date())
    # @animate()

    @animationId = requestAnimationFrame(@animationFrameProxy)

  onResize: (e) ->
    return
    # deprecated
    #@clockElement.style.height = @clockElement.clientWidth + 'px'


# only 
class ccmm.CalendarClock
  @currentEvents = {}
  @calendars = []
  # '1395853200|1395856800|121,129,93,0.5|jeffa@6ft.com|Weekly Update (Hot Topics)'

  constructor: (@eventCanvas, @clock, @eventsListElement, @options, @theme) ->
    ccmm.CalendarClock.calendars.push(@)
    @clock.plugins.push(@)
    @draw()

  draw: ->
    if @eventsListElement
      listedEvents = @eventsListElement.querySelectorAll('.event-item')
    else
      listedEvents = []
    foundEvents = []
    previousColor = 'white'
    colorChanges = 0
    colorStayed = 0

    currentTime = new Date()
    tzOffset = 0
    if currentTime.isDST()
      tzOffset = -3600000
    for event, i in ccmm.CalendarClock.currentEvents
      event_start = (event.start * 1000) + tzOffset
      event_end = (event.end * 1000) + tzOffset
      if event.isAllDay
        @clock.drawTimeSpanArc(event_start, event_end, ARC_RADIUS - ARC_THICKNESS + (ARC_THICKNESS/4) - (colorChanges), 1, @theme.allDayEventColor, @theme.clearColor)
      else
        @clock.drawTimeSpanArc(event_start, event_end, ARC_RADIUS - ARC_THICKNESS - ((colorChanges)*5), 3, event.color, @theme.clearColor)
        # console.log( ARC_RADIUS - ARC_THICKNESS - ((colorChanges)*5))

      if event.color != previousColor
        colorChanges = colorChanges + 1 
        colorStayed = 0
      else
        colorStayed = colorStayed + 1

      # draw calendar events for today.
      previousColor = event.color

      exists = false
      for eventItem in listedEvents
        if eventItem.getAttribute('data-description') == event.description and eventItem.getAttribute('data-name') == event.name
          foundEvents.push(event.name+'|'+event.description)
          exists = true

      if not exists
        # add it
        newItem = document.createElement('div')
        foundEvents.push(event.name+'|'+event.description)
        newItem.classList.add('event-item')
        newItem.setAttribute('data-description', event.description)
        newItem.setAttribute('data-name', event.name)
        newItem.setAttribute('data-start', event.start)
        newItem.setAttribute('data-end', event.end)
        newItem.setAttribute('data-color', event.color)
        newItem.style.color = event.color;
        start = new Date(event_start)
        end = new Date(event_end)
        hour = prettyHour(start)
        minute = prettyMinute(start)
        ampm = prettyAMPM(start)
        newItem.innerHTML = "<div class=\"who\">#{event.name}</div><div class=\"desc\">#{hour}:#{minute} #{ampm} #{event.description}</div>"
        if @eventsListElement
          @eventsListElement.appendChild(newItem)
          listedEvents = @eventsListElement.querySelectorAll('.event-item')
        else
          listedEvents = []
      
    # listedEvents = @eventsListElement.querySelectorAll('.event-item')
    # for eventItem in listedEvents
      
    return

class ccmm.Client
  class ClientMessage
    constructor: (@type, @data) ->
      return

  constructor: (@host, @port, @statusIndicatorElement) ->
    @socket = new WebSocket("ws://#{@host}:#{@port}")
    @socket.addEventListener('open', @onOpen.bind(@))
    @socket.addEventListener('message', @onMessage.bind(@))

  ping: ->
    message = new ClientMessage('ping', new Date().valueOf())
    @socket.send(JSON.stringify(message))

  pollEvents: ->
    message = new ClientMessage('update-events', null)
    @socket.send(JSON.stringify(message))

  onOpen: ->
    message = new ClientMessage('connected', null)
    @socket.send(JSON.stringify(message))

    setInterval(@ping.bind(@), 5000)
    @ping()

    # TODO: scalable
    message = new ClientMessage('get-data', {'plugin': 'externals'})
    @socket.send(JSON.stringify(message))
    message = new ClientMessage('get-data', {'plugin': 'internals'})
    @socket.send(JSON.stringify(message))
    message = new ClientMessage('get-data', {'plugin': 'pollen'})
    @socket.send(JSON.stringify(message))

  onMessage: (response, flags) ->
    data = JSON.parse(response.data)
    switch data['type']
      when 'pong'
        @statusIndicatorElement.classList.add('pulse')
        document.body.clientWidth
        @statusIndicatorElement.classList.remove('pulse')
      when 'events'
        if (data['data'])
          ccmm.CalendarClock.currentEvents = data['data']
          for cal in ccmm.CalendarClock.calendars
            cal.draw()
      when 'externals'
        console.log(response.data);
        if (data['data'] and window.env.weatherChart)
          weatherHistory = []
          temp = 0
          humidity = 0
          description = 'N/A'
          for history in data['data']
            weatherHistory.push(history.temp)
            # stor the last one
            temp = history.temp
            humidity = history.humidity
            description = history.description
          
          env.weatherChart.setData(weatherHistory)
          env.weatherChart.setCurrentValue(Math.floor(temp), Math.floor(humidity), description)

      when 'internals'
        if (data['data'] and window.env.weatherChart)
          themostatHistory = []
          temp = 0
          for history in data['data']
            themostatHistory.push(history['temp'])
            temp = history.temp
          
          env.ambientChart.setData(themostatHistory)
          env.ambientChart.setCurrentValue(Math.floor(temp), 76, '0 mins')
      when 'pollen'
        if (data['data'] and window.env.weatherChart)
          pollenCounts = []
          pollen = 0
          type = 'N/A'
          for count in data['data']
            pollenCounts.push(count['count'])
            pollen = count.count
            type = count.description
          
          env.pollenChart.setData(pollenCounts)
          env.pollenChart.setCurrentValue(pollen, type)
        


  onDisconnect: ->
    # reconnect, always with the connections
    @socket = null
    @socket = new WebSocket("ws://#{@host}:#{@port}")
    @socket.addEventListener('open', @onOpen.bind(@))
    @socket.addEventListener('message', @onMessage.bind(@))
    return


