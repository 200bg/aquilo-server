(function() {
  var ARC_RADIUS, ARC_THICKNESS, CANVAS_ARC_OFFSET_RADIAN, CLOCK_HEIGHT, CLOCK_RADIUS, CLOCK_START, CLOCK_WIDTH, FULL_CIRCLE, GLOW_ARC_THICKNESS, HALF_CIRCLE, HOUR_RADIAN, HOUR_TO_RESET, ICON_PADDING, ICON_SIZE, LATITUDE, LONGITUDE, MILLISECONDS_IN_A_DAY, MILLISECOND_RADIAN, MINUTE_RADIAN, MONTHS, OFFSET_X, OFFSET_Y, QUARTER_CIRCLE, REFRESH_RATE, SECOND_RADIAN, SEGMENTS, TEXT_ICON_PADDING, TICK_SIZE, alphaRegex, currentRotationOffset, currentTime, extractAlpha, prettyAMPM, prettyHour, prettyMinute,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.ccmm = window.ccmm || {};

  FULL_CIRCLE = Math.PI * 2;

  HALF_CIRCLE = Math.PI;

  QUARTER_CIRCLE = Math.PI / 2;

  HOUR_RADIAN = FULL_CIRCLE / 24;

  MINUTE_RADIAN = FULL_CIRCLE / (24 * 60);

  SECOND_RADIAN = FULL_CIRCLE / (24 * 60 * 60);

  MILLISECOND_RADIAN = FULL_CIRCLE / (24 * 60 * 60 * 1000);

  SEGMENTS = 12;

  MILLISECONDS_IN_A_DAY = 86400000;

  CANVAS_ARC_OFFSET_RADIAN = Math.PI / 2;

  CLOCK_START = Math.PI;

  currentRotationOffset = 0;

  currentTime = new Date();

  alphaRegex = /,([\d\.]+)\)$/m;

  extractAlpha = function(color) {
    if (color.indexOf('rgba') === 0) {
      return Number(color.match(alphaRegex)[1]);
    }
    return 1.0;
  };

  prettyHour = function(time) {
    var hour;
    hour = time.getHours();
    if (hour > 12) {
      hour = hour - 12;
    }
    if (hour === 0) {
      hour = 12;
    }
    if (hour < 10) {
      hour = '0' + hour;
    }
    return hour;
  };

  prettyMinute = function(time) {
    var minute;
    minute = time.getMinutes();
    if (minute < 10) {
      minute = '0' + minute;
    }
    return minute;
  };

  prettyAMPM = function(time) {
    var hour;
    hour = time.getHours();
    if (hour >= 12) {
      return 'PM';
    }
    return 'AM';
  };

  ICON_SIZE = 12;

  ICON_PADDING = -5;

  TEXT_ICON_PADDING = -4;

  CLOCK_WIDTH = 390;

  CLOCK_HEIGHT = 390;

  CLOCK_RADIUS = Math.floor(CLOCK_WIDTH / 2);

  OFFSET_X = 127;

  OFFSET_Y = 70;

  ARC_RADIUS = CLOCK_RADIUS;

  ARC_THICKNESS = 40;

  GLOW_ARC_THICKNESS = 40;

  TICK_SIZE = 12;

  LATITUDE = 29.681416;

  LONGITUDE = -95.383172;

  REFRESH_RATE = 60;

  HOUR_TO_RESET = 20;

  MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  ccmm.Tabset = (function() {
    function Tabset(element, sections) {
      var tab, _i, _len, _ref;
      this.element = element;
      this.sections = sections;
      this.tabs = this.element.querySelectorAll('.tab');
      this.onClickProxy = this.onClick.bind(this);
      _ref = this.tabs;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tab = _ref[_i];
        tab.addEventListener('click', this.onClickProxy);
      }
    }

    Tabset.prototype.activate = function(name) {
      var tab, tabName, _i, _len, _ref, _results;
      _ref = this.tabs;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tab = _ref[_i];
        tabName = tab.getAttribute('data-name');
        if (tabName === name) {
          this.sections[name].activate();
          _results.push(tab.classList.add('active'));
        } else {
          this.sections[tabName].deactivate();
          _results.push(tab.classList.remove('active'));
        }
      }
      return _results;
    };

    Tabset.prototype.onClick = function(e) {
      var tab, tabName;
      tab = e.currentTarget;
      tabName = tab.getAttribute('data-name');
      return this.activate(tabName);
    };

    return Tabset;

  })();

  ccmm.Section = (function() {
    function Section(element) {
      this.element = element;
      this.isActive = false;
    }

    Section.prototype.activate = function() {
      this.isActive = true;
      return this.element.style.display = null;
    };

    Section.prototype.deactivate = function() {
      this.isActive = false;
      return this.element.style.display = 'none';
    };

    return Section;

  })();

  ccmm.Clock = (function(_super) {
    __extends(Clock, _super);

    function Clock(clockElement, baseCanvas, clockCanvas, animationCanvas, timeElement, dateElement, data, options, theme) {
      this.clockElement = clockElement;
      this.baseCanvas = baseCanvas;
      this.clockCanvas = clockCanvas;
      this.animationCanvas = animationCanvas;
      this.timeElement = timeElement;
      this.dateElement = dateElement;
      this.data = data;
      this.options = options != null ? options : {};
      this.theme = theme != null ? theme : ccmm.themes['tech-49'];
      Clock.__super__.constructor.call(this, this.clockElement);
      this.plugins = [];
      ICON_SIZE = this.options.ICON_SIZE || ICON_SIZE;
      ICON_PADDING = this.options.ICON_PADDING || ICON_PADDING;
      TEXT_ICON_PADDING = this.options.TEXT_ICON_PADDING || TEXT_ICON_PADDING;
      CLOCK_WIDTH = this.options.CLOCK_WIDTH || CLOCK_WIDTH;
      CLOCK_HEIGHT = this.options.CLOCK_HEIGHT || CLOCK_HEIGHT;
      CLOCK_RADIUS = this.options.CLOCK_RADIUS || CLOCK_RADIUS;
      OFFSET_X = this.options.OFFSET_X || OFFSET_X;
      OFFSET_Y = this.options.OFFSET_Y || OFFSET_Y;
      ARC_RADIUS = this.options.ARC_RADIUS || ARC_RADIUS;
      ARC_THICKNESS = this.options.ARC_THICKNESS || ARC_THICKNESS;
      GLOW_ARC_THICKNESS = this.options.GLOW_ARC_THICKNESS || GLOW_ARC_THICKNESS;
      TICK_SIZE = this.options.TICK_SIZE || TICK_SIZE;
      LATITUDE = this.options.LATITUDE || LATITUDE;
      LONGITUDE = this.options.LONGITUDE || LONGITUDE;
      HOUR_TO_RESET = this.options.HOUR_TO_RESET || HOUR_TO_RESET;
      this.animationContext = this.animationCanvas.getContext('2d');
      this.drawingCanvas = document.createElement('canvas');
      this.drawingCanvas.width = this.clockCanvas.width;
      this.drawingCanvas.height = this.clockCanvas.height;
      this.context = this.drawingCanvas.getContext('2d');
      this.outputContext = this.clockCanvas.getContext('2d');
      this.centerPoint = this.clockCanvas.width / 2;
      this.currentFrame = 59;
      this.pulseFrame = 0;
      this.lat = this.data.latitude;
      this.lon = this.data.longitude;
      this.currentTime = new Date();
      this.currentDate = this.currentTime.getDate();
      this.currentRotationOffset = QUARTER_CIRCLE + this.getRadianForTime(this.currentTime, this.getMidnight(this.currentTime));
      this.monthElement = this.dateElement.querySelector('.month');
      this.dayElement = this.dateElement.querySelector('.day');
      this.timeElement.style.color = this.theme.dateTimeColor;
      this.monthElement.style.color = this.theme.dateMonthColor;
      this.dayElement.style.color = this.theme.dateDayColor;
      window.addEventListener('resize', this.onResize.bind(this));
      this.resetSolars();
      this.sunriseRadian = this.getRadianForTime(this.sunrise);
      this.sunsetRadian = this.getRadianForTime(this.sunset);
      this.animationFrameProxy = this.onAnimationFrameTick.bind(this);
      this.animationId = 0;
      this.start();
      this.drawBase(this.context, 0);
      this.setTime(this.currentTime);
      this.needsReset = false;
      this.hasUpdates = false;
    }

    Clock.prototype.start = function() {
      cancelAnimationFrame(this.animationId);
      return this.animationId = requestAnimationFrame(this.animationFrameProxy);
    };

    Clock.prototype.stop = function() {
      return cancelAnimationFrame(this.animationId);
    };

    Clock.prototype.activate = function() {
      Clock.__super__.activate.call(this);
      this.clockElement.classList.add('animated-out');
      document.body.clientWidth;
      this.clockElement.classList.remove('animated-out');
      return this.start();
    };

    Clock.prototype.deactivate = function() {
      Clock.__super__.deactivate.call(this);
      return this.stop();
    };

    Clock.prototype.resetSolars = function() {
      var tzOffset;
      tzOffset = this.currentTime.getTimezoneOffset() / -60;
      if (this.currentTime.isDST()) {
        tzOffset -= 1;
      }
      this.sunrise = noaa.solar.calculateSunrise(this.currentTime, this.lat, this.lon, tzOffset, this.currentTime.isDST());
      this.sunset = noaa.solar.calculateSunset(this.currentTime, this.lat, this.lon, tzOffset, this.currentTime.isDST());
      return this.noon = noaa.solar.calculateSolarNoon(this.currentTime, this.lon, tzOffset, this.currentTime.isDST());
    };

    Clock.prototype.getMidnight = function(time) {
      var midnight;
      midnight = new Date(time.valueOf());
      return midnight.setHours(0, 0, 0, 0);
    };

    Clock.prototype.setTime = function(currentTime) {
      var date, err, hour, minute, plugin, _i, _len, _ref;
      this.currentTime = currentTime;
      this.currentRotationOffset = QUARTER_CIRCLE + this.getRadianForTime(this.currentTime, this.getMidnight(this.currentTime));
      this.outputContext.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);
      this.redraw(-this.currentRotationOffset);
      _ref = this.plugins;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        plugin = _ref[_i];
        try {
          plugin.draw(this.currentTime);
        } catch (_error) {
          err = _error;
          return;
        }
      }
      this.currentFrame = 0;
      hour = prettyHour(this.currentTime);
      minute = prettyMinute(this.currentTime);
      this.timeElement.innerHTML = hour + ':' + minute;
      date = this.currentTime.getDate();
      if (date < 10) {
        date = '0' + date;
      }
      this.monthElement.innerHTML = MONTHS[this.currentTime.getMonth()];
      return this.dayElement.innerHTML = date;
    };

    Clock.prototype.getRadianForTime = function(dateTime, relativeTo) {
      var angleForTime, duration, percentOfArc;
      if (relativeTo == null) {
        relativeTo = this.currentTime;
      }
      duration = dateTime - relativeTo;
      percentOfArc = duration / MILLISECONDS_IN_A_DAY;
      angleForTime = percentOfArc * FULL_CIRCLE;
      return angleForTime;
    };

    Clock.prototype.clear = function() {
      return this.context.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);
    };

    Clock.prototype.redraw = function(offset) {
      this.outputContext.save();
      this.outputContext.translate(this.clockCanvas.width / 2, this.clockCanvas.height / 2);
      this.outputContext.rotate(offset);
      this.outputContext.translate(this.clockCanvas.width / -2, this.clockCanvas.height / -2);
      this.outputContext.drawImage(this.drawingCanvas, 0, 0);
      this.outputContext.restore();
      this.outputContext.strokeStyle = this.theme.iconColor;
      this.outputContext.lineWidth = 1;
      this.outputContext.beginPath();
      this.outputContext.moveTo(this.centerPoint, this.centerPoint - ARC_RADIUS - ARC_THICKNESS - (ICON_SIZE + ICON_PADDING));
      this.outputContext.lineTo(this.centerPoint, this.centerPoint - ARC_RADIUS - ARC_THICKNESS - ICON_PADDING);
      this.outputContext.closePath();
      return this.outputContext.stroke();
    };

    Clock.prototype.drawBase = function(context, rotationOffset) {
      var diff, formattedHour, hour, segmentStart, _i, _results;
      if (rotationOffset == null) {
        rotationOffset = 0;
      }
      context.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);
      context.globalCompositeOperation = 'source-over';
      this.drawTimeSpanArc(this.sunrise.valueOf(), this.sunset.valueOf(), ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.dayArcColor, this.theme.dayArcColor);
      this.drawSegmentArcs(this.sunrise, this.sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.glowColor, extractAlpha(this.theme.glowColor), false, 64, rotationOffset);
      diff = MILLISECONDS_IN_A_DAY - this.sunset.valueOf() + (this.sunrise.valueOf());
      this.drawTimeSpanArc(this.sunset.valueOf(), this.sunset + diff.valueOf(), ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.nightArcColor, this.theme.nightArcColor);
      this.drawSegmentArcs(this.sunset, this.sunset + diff, ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.nightColor, true, SEGMENTS, rotationOffset);
      context.globalCompositeOperation = 'screen';
      segmentStart = this.getMidnight(this.currentTime) + (MILLISECONDS_IN_A_DAY / 4) + (MILLISECONDS_IN_A_DAY / 8);
      this.drawSegmentArcs(segmentStart, segmentStart + (MILLISECONDS_IN_A_DAY / 2), ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.dayColor, 0.33, false, SEGMENTS, rotationOffset);
      context.globalCompositeOperation = 'source-over';
      this.drawSegmentArcs(segmentStart + (MILLISECONDS_IN_A_DAY / 2), segmentStart + MILLISECONDS_IN_A_DAY - 1, ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.nightColor, extractAlpha(this.theme.nightColor), true, SEGMENTS, rotationOffset);
      _results = [];
      for (hour = _i = 1; _i <= 24; hour = ++_i) {
        if (hour % 6 === 0) {
          if (hour % 12 === 0) {
            this.drawTickAtRadian(this.context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 10, this.theme.baseColor);
          } else {
            this.drawTickAtRadian(this.context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 6, this.theme.baseColor);
          }
        } else {
          this.drawTickAtRadian(this.context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 2, this.theme.baseColor);
        }
        if (hour % 12 === 0) {
          formattedHour = 12;
        } else {
          formattedHour = hour % 12;
        }
        _results.push(this.drawTextAtRadian(this.context, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE, 20, formattedHour, this.theme.glowColor, rotationOffset));
      }
      return _results;
    };

    Clock.prototype.animate = function() {
      return this.animationContext.clearRect(0, 0, this.animationCanvas.width, this.animationCanvas.height);
    };

    Clock.prototype.drawSegmentArcs = function(startDate, endDate, radius, size, arcColor, colorAlpha, invertGradient, segments, rotationOffset) {
      var alpha, halfSegments, i, interval, opacityInterval, span, _i, _ref, _results;
      if (segments == null) {
        segments = SEGMENTS;
      }
      span = endDate.valueOf() - startDate.valueOf();
      interval = Math.floor(span / segments);
      halfSegments = Math.floor(segments / 2);
      opacityInterval = colorAlpha / halfSegments;
      alpha = 1.0;
      _results = [];
      for (i = _i = 0, _ref = segments - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        if (i < halfSegments) {
          alpha = opacityInterval * (i + 1.0);
        } else {
          alpha = opacityInterval * (halfSegments - ((i + 1.0) - halfSegments));
        }
        this.context.globalAlpha = alpha;
        this.drawTimeSpanArc(startDate.valueOf() + (interval * i), startDate.valueOf() + (interval * (i + 1)), radius, size, arcColor, rotationOffset);
        _results.push(this.context.globalAlpha = 1.0);
      }
      return _results;
    };

    Clock.prototype.drawTickAtRadian = function(context, radian, size, color) {
      var ex, ey, sx, sy;
      context.strokeStyle = color;
      context.lineWidth = 1;
      context.beginPath();
      sx = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING)) * Math.sin(radian);
      sy = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING)) * Math.cos(radian);
      ex = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + ICON_PADDING) * Math.sin(radian);
      ey = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + ICON_PADDING) * Math.cos(radian);
      context.moveTo(sx, sy);
      context.lineTo(ex, ey);
      context.closePath();
      return context.stroke();
    };

    Clock.prototype.drawTextAtRadian = function(context, radian, size, text, color, rotationOffset) {
      var centerOffset, charCount, rotation, sx, sy;
      context.fillStyle = color;
      context.textAlign = 'center';
      charCount = text.length;
      centerOffset = 0;
      if (charCount > 1) {
        centerOffset = Math.floor(size / 2) * (Math.floor(charCount / 2));
      } else {
        centerOffset = Math.floor(size / 4);
      }
      sx = centerOffset;
      sy = (ARC_RADIUS + ARC_THICKNESS + TEXT_ICON_PADDING) * -1;
      rotation = (Math.PI * 2) - (rotationOffset + radian);
      context.translate(this.centerPoint, this.centerPoint);
      context.rotate(rotation);
      context.moveTo(sx, sy);
      context.fillText(text, sx, sy, size);
      context.moveTo(0, 0);
      context.rotate(-rotation);
      return context.translate(-this.centerPoint, -this.centerPoint);
    };

    Clock.prototype.drawTimeSpanArc = function(startDate, endDate, radius, size, color, rotationOffset) {
      var endAngle, endDateRadian, ex, ey, startAngle, startDateRadian, sx, sy;
      this.context.strokeStyle = color;
      this.context.lineWidth = size;
      this.context.beginPath();
      startDateRadian = this.getRadianForTime(startDate, this.getMidnight(this.currentTime));
      endDateRadian = this.getRadianForTime(endDate, this.getMidnight(this.currentTime));
      sx = this.centerPoint + Math.sin(rotationOffset + QUARTER_CIRCLE - startDateRadian) * radius;
      sy = this.centerPoint + Math.cos(rotationOffset + QUARTER_CIRCLE - startDateRadian) * radius;
      ex = this.centerPoint + Math.sin(rotationOffset + QUARTER_CIRCLE - endDateRadian) * radius;
      ey = this.centerPoint + Math.cos(rotationOffset + QUARTER_CIRCLE - endDateRadian) * radius;
      startAngle = Math.atan2(this.centerPoint - sy, this.centerPoint - sx);
      endAngle = Math.atan2(this.centerPoint - ey, this.centerPoint - ex);
      this.context.arc(this.centerPoint, this.centerPoint, radius, startAngle - HALF_CIRCLE, endAngle - HALF_CIRCLE, false);
      this.context.moveTo(ex, ey);
      this.context.closePath();
      return this.context.stroke();
    };

    Clock.prototype.drawSunPathArcs = function(startDate, endDate, radius, size, arcColor, rotationOffset) {
      var baseColor, color, halfSegments, i, interval, opacityInterval, span, _i, _ref, _results;
      if (rotationOffset == null) {
        rotationOffset = 0;
      }
      span = endDate - startDate;
      interval = Math.floor(span / SEGMENTS);
      baseColor = arcColor.replace(/[rgba]+?\((.+)\)/, '$1').split(',');
      halfSegments = Math.floor(SEGMENTS / 2);
      opacityInterval = baseColor[3] / halfSegments;
      _results = [];
      for (i = _i = 0, _ref = SEGMENTS - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        if (i < halfSegments) {
          color = "rgba(" + baseColor[0] + "," + baseColor[1] + "," + baseColor[2] + "," + (opacityInterval * (i + 1)) + ")";
        } else {
          color = "rgba(" + baseColor[0] + "," + baseColor[1] + "," + baseColor[2] + "," + (opacityInterval * (halfSegments - ((i + 1) - halfSegments))) + ")";
        }
        _results.push(this.drawTimeSpanArc(startDate.valueOf() + (interval * i), startDate.valueOf() + (interval * (i + 1)), radius, size, color, this.theme.clearColor));
      }
      return _results;
    };

    Clock.prototype.onAnimationFrameTick = function(tickTime) {
      if ((this.currentTime.getHours() === HOUR_TO_RESET && this.needsReset) || this.currentTime.getDate() !== this.currentDate) {
        this.needsReset = false;
        this.resetSolars();
        this.drawBase(this.context, 0);
        this.currentDate = this.currentTime.getDate();
      } else if (this.currentTime.getHours() !== HOUR_TO_RESET) {
        this.needsReset = true;
      }
      this.currentFrame++;
      if (this.currentFrame >= REFRESH_RATE) {
        this.setTime(new Date());
      }
      return this.animationId = requestAnimationFrame(this.animationFrameProxy);
    };

    Clock.prototype.onResize = function(e) {};

    return Clock;

  })(ccmm.Section);

  ccmm.CalendarClock = (function() {
    CalendarClock.currentEvents = {};

    CalendarClock.calendars = [];

    function CalendarClock(eventCanvas, clock, eventsListElement, options, theme) {
      this.eventCanvas = eventCanvas;
      this.clock = clock;
      this.eventsListElement = eventsListElement;
      this.options = options;
      this.theme = theme;
      ccmm.CalendarClock.calendars.push(this);
      this.clock.plugins.push(this);
      this.draw();
    }

    CalendarClock.prototype.draw = function() {
      var ampm, colorChanges, colorStayed, end, event, eventItem, event_end, event_start, exists, foundEvents, hour, i, listedEvents, minute, newItem, previousColor, start, tzOffset, _i, _j, _len, _len1, _ref;
      if (this.eventsListElement) {
        listedEvents = this.eventsListElement.querySelectorAll('.event-item');
      } else {
        listedEvents = [];
      }
      foundEvents = [];
      previousColor = 'white';
      colorChanges = 0;
      colorStayed = 0;
      currentTime = new Date();
      tzOffset = 0;
      if (currentTime.isDST()) {
        tzOffset = -3600000;
      }
      _ref = ccmm.CalendarClock.currentEvents;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        event = _ref[i];
        event_start = (event.start * 1000) + tzOffset;
        event_end = (event.end * 1000) + tzOffset;
        if (event.isAllDay) {
          this.clock.drawTimeSpanArc(event_start, event_end, ARC_RADIUS - ARC_THICKNESS + (ARC_THICKNESS / 4) - colorChanges, 1, this.theme.allDayEventColor, this.theme.clearColor);
        } else {
          this.clock.drawTimeSpanArc(event_start, event_end, ARC_RADIUS - ARC_THICKNESS - (colorChanges * 5), 3, event.color, this.theme.clearColor);
        }
        if (event.color !== previousColor) {
          colorChanges = colorChanges + 1;
          colorStayed = 0;
        } else {
          colorStayed = colorStayed + 1;
        }
        previousColor = event.color;
        exists = false;
        for (_j = 0, _len1 = listedEvents.length; _j < _len1; _j++) {
          eventItem = listedEvents[_j];
          if (eventItem.getAttribute('data-description') === event.description && eventItem.getAttribute('data-name') === event.name) {
            foundEvents.push(event.name + '|' + event.description);
            exists = true;
          }
        }
        if (!exists) {
          newItem = document.createElement('div');
          foundEvents.push(event.name + '|' + event.description);
          newItem.classList.add('event-item');
          newItem.setAttribute('data-description', event.description);
          newItem.setAttribute('data-name', event.name);
          newItem.setAttribute('data-start', event.start);
          newItem.setAttribute('data-end', event.end);
          newItem.setAttribute('data-color', event.color);
          newItem.style.color = event.color;
          start = new Date(event_start);
          end = new Date(event_end);
          hour = prettyHour(start);
          minute = prettyMinute(start);
          ampm = prettyAMPM(start);
          newItem.innerHTML = "<div class=\"who\">" + event.name + "</div><div class=\"desc\">" + hour + ":" + minute + " " + ampm + " " + event.description + "</div>";
          if (this.eventsListElement) {
            this.eventsListElement.appendChild(newItem);
            listedEvents = this.eventsListElement.querySelectorAll('.event-item');
          } else {
            listedEvents = [];
          }
        }
      }
    };

    return CalendarClock;

  })();

  ccmm.Client = (function() {
    var ClientMessage;

    ClientMessage = (function() {
      function ClientMessage(type, data) {
        this.type = type;
        this.data = data;
        return;
      }

      return ClientMessage;

    })();

    function Client(host, port, statusIndicatorElement) {
      this.host = host;
      this.port = port;
      this.statusIndicatorElement = statusIndicatorElement;
      this.socket = new WebSocket("ws://" + this.host + ":" + this.port);
      this.socket.addEventListener('open', this.onOpen.bind(this));
      this.socket.addEventListener('message', this.onMessage.bind(this));
    }

    Client.prototype.ping = function() {
      var message;
      message = new ClientMessage('ping', new Date().valueOf());
      return this.socket.send(JSON.stringify(message));
    };

    Client.prototype.pollEvents = function() {
      var message;
      message = new ClientMessage('update-events', null);
      return this.socket.send(JSON.stringify(message));
    };

    Client.prototype.onOpen = function() {
      var message;
      message = new ClientMessage('connected', null);
      this.socket.send(JSON.stringify(message));
      setInterval(this.ping.bind(this), 5000);
      this.ping();
      message = new ClientMessage('get-data', {
        'plugin': 'externals'
      });
      this.socket.send(JSON.stringify(message));
      message = new ClientMessage('get-data', {
        'plugin': 'internals'
      });
      this.socket.send(JSON.stringify(message));
      message = new ClientMessage('get-data', {
        'plugin': 'pollen'
      });
      return this.socket.send(JSON.stringify(message));
    };

    Client.prototype.onMessage = function(response, flags) {
      var cal, count, data, description, history, humidity, pollen, pollenCounts, temp, themostatHistory, type, weatherHistory, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref, _ref1, _ref2, _ref3, _results;
      data = JSON.parse(response.data);
      switch (data['type']) {
        case 'pong':
          this.statusIndicatorElement.classList.add('pulse');
          document.body.clientWidth;
          return this.statusIndicatorElement.classList.remove('pulse');
        case 'events':
          if (data['data']) {
            ccmm.CalendarClock.currentEvents = data['data'];
            _ref = ccmm.CalendarClock.calendars;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              cal = _ref[_i];
              _results.push(cal.draw());
            }
            return _results;
          }
          break;
        case 'externals':
          console.log(response.data);
          if (data['data'] && window.env.weatherChart) {
            weatherHistory = [];
            temp = 0;
            humidity = 0;
            description = 'N/A';
            _ref1 = data['data'];
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              history = _ref1[_j];
              weatherHistory.push(history.temp);
              temp = history.temp;
              humidity = history.humidity;
              description = history.description;
            }
            env.weatherChart.setData(weatherHistory);
            return env.weatherChart.setCurrentValue(Math.floor(temp), Math.floor(humidity), description);
          }
          break;
        case 'internals':
          if (data['data'] && window.env.weatherChart) {
            themostatHistory = [];
            temp = 0;
            _ref2 = data['data'];
            for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
              history = _ref2[_k];
              themostatHistory.push(history['temp']);
              temp = history.temp;
            }
            env.ambientChart.setData(themostatHistory);
            return env.ambientChart.setCurrentValue(Math.floor(temp), 76, '0 mins');
          }
          break;
        case 'pollen':
          if (data['data'] && window.env.weatherChart) {
            pollenCounts = [];
            pollen = 0;
            type = 'N/A';
            _ref3 = data['data'];
            for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
              count = _ref3[_l];
              pollenCounts.push(count['count']);
              pollen = count.count;
              type = count.description;
            }
            env.pollenChart.setData(pollenCounts);
            return env.pollenChart.setCurrentValue(pollen, type);
          }
      }
    };

    Client.prototype.onDisconnect = function() {
      this.socket = null;
      this.socket = new WebSocket("ws://" + this.host + ":" + this.port);
      this.socket.addEventListener('open', this.onOpen.bind(this));
      this.socket.addEventListener('message', this.onMessage.bind(this));
    };

    return Client;

  })();

}).call(this);
