//# sourceMappingURL=oblique.js.map
(function() {
  window.oblique = {};

  oblique.themes = {
    "oblivion": {
      background: 'rgba(0,0,0,1.0)',
      glowColor: 'rgba(92,190,209,0.33)',
      faintColor: 'rgba(92,190,209,0.1)',
      nightColor: 'rgba(92,190,209,0.1)',
      dayColor: 'rgba(92,190,209,0.8)',
      baseColor: 'rgba(92,190,209,0.1)',
      iconColor: 'rgba(92,190,209,0.5)',
      highlightColor: 'rgba(92,190,209,0.8)',
      alertColor: 'rgba(255,0,0,0.75)',
      clearColor: 'rgba(0, 0, 0, 0.0)'
    }
  };

  oblique.Clock = (function() {
    var ARC_RADIUS, ARC_THICKNESS, CANVAS_ARC_OFFSET_RADIAN, CLOCK_START, FULL_CIRCLE, GLOW_ARC_THICKNESS, HALF_CIRCLE, HOUR_RADIAN, ICON_PADDING, ICON_SIZE, MILLISECONDS_IN_A_DAY, MILLISECOND_RADIAN, MINUTE_RADIAN, PULSE_RATE, QUARTER_CIRCLE, REFRESH_RATE, SECOND_RADIAN, SEGMENTS;

    FULL_CIRCLE = Math.PI * 2;

    HALF_CIRCLE = Math.PI;

    QUARTER_CIRCLE = Math.PI / 2;

    HOUR_RADIAN = FULL_CIRCLE / 24;

    MINUTE_RADIAN = FULL_CIRCLE / (24 * 60);

    SECOND_RADIAN = FULL_CIRCLE / (24 * 60 * 60);

    MILLISECOND_RADIAN = FULL_CIRCLE / (24 * 60 * 60 * 1000);

    SEGMENTS = 14;

    MILLISECONDS_IN_A_DAY = 86400000;

    CANVAS_ARC_OFFSET_RADIAN = Math.PI / 2;

    CLOCK_START = Math.PI;

    ARC_RADIUS = 180.0;

    ARC_THICKNESS = 30;

    GLOW_ARC_THICKNESS = 32;

    ICON_SIZE = 12;

    ICON_PADDING = 2;

    REFRESH_RATE = 180;

    PULSE_RATE = REFRESH_RATE * 10;

    function Clock(baseCanvas, clockCanvas, animationCanvas, data, options, theme) {
      var tzOffset;
      this.baseCanvas = baseCanvas;
      this.clockCanvas = clockCanvas;
      this.animationCanvas = animationCanvas;
      this.data = data;
      this.options = options;
      this.theme = theme;
      this.animationContext = this.animationCanvas.getContext('2d');
      this.drawingCanvas = document.createElement('canvas');
      this.drawingCanvas.width = this.clockCanvas.width;
      this.drawingCanvas.height = this.clockCanvas.height;
      this.context = this.drawingCanvas.getContext('2d');
      this.outputContext = this.clockCanvas.getContext('2d');
      this.centerPoint = this.clockCanvas.width / 2;
      this.currentFrame = 59;
      this.pulseFrame = 0;
      this.lat = this.data.latitude;
      this.lon = this.data.longitude;
      this.currentTime = new Date();
      this.currentRotationOffset = QUARTER_CIRCLE + this.getRadianForTime(this.currentTime, this.getMidnight(this.currentTime));
      tzOffset = this.currentTime.getTimezoneOffset() / -60;
      if (this.currentTime.isDST()) {
        tzOffset--;
      }
      console.log(tzOffset);
      this.sunrise = noaa.solar.calculateSunrise(this.currentTime, this.lat, this.lon, tzOffset, this.currentTime.isDST());
      this.sunset = noaa.solar.calculateSunset(this.currentTime, this.lat, this.lon, tzOffset, this.currentTime.isDST());
      this.noon = noaa.solar.calculateSolarNoon(this.currentTime, this.lon, tzOffset, this.currentTime.isDST());
      this.sunriseRadian = this.getRadianForTime(this.sunrise);
      this.sunsetRadian = this.getRadianForTime(this.sunset);
      this.animationFrameProxy = this.onAnimationFrameTick.bind(this);
      this.demoAnimationFrameProxy = this.onDemoAnimationFrameTick.bind(this);
      this.animationId = 0;
      this.start();
      this.drawBase();
      this.draw();
      this.hasUpdates = false;
    }

    Clock.prototype.start = function() {
      cancelAnimationFrame(this.animationId);
      return this.animationId = requestAnimationFrame(this.animationFrameProxy);
    };

    Clock.prototype.stop = function() {
      return cancelAnimationFrame(this.animationId);
    };

    Clock.prototype.startDemo = function() {
      var now;
      cancelAnimationFrame(this.animationId);
      now = new Date();
      this.currentTime = new Date(now.getYear(), now.getMonth(), now.getDate(), 0, 0, 0);
      console.log(this.currentTime);
      return this.animationId = requestAnimationFrame(this.demoAnimationFrameProxy);
    };

    Clock.prototype.getMidnight = function(time) {
      var midnight;
      midnight = new Date(time.valueOf());
      return midnight.setHours(0, 0, 0, 0);
    };

    Clock.prototype.setTime = function(currentTime) {
      var formattedHour, hour, _i, _results;
      this.currentTime = currentTime;
      this.currentRotationOffset = QUARTER_CIRCLE + this.getRadianForTime(this.currentTime, this.getMidnight(this.currentTime));
      this.outputContext.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);
      this.outputContext.save();
      this.outputContext.translate(this.centerPoint, this.centerPoint);
      this.outputContext.rotate(-this.currentRotationOffset);
      this.outputContext.translate(-this.centerPoint, -this.centerPoint);
      this.outputContext.drawImage(this.drawingCanvas, 0, 0);
      this.currentFrame = 0;
      this.outputContext.restore();
      _results = [];
      for (hour = _i = 1; _i <= 24; hour = ++_i) {
        if (hour % 6 === 0) {
          if (hour % 12 === 0) {
            this.drawTickAtRadian(this.outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + this.currentRotationOffset, 10, this.theme.baseColor);
          } else {
            this.drawTickAtRadian(this.outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + this.currentRotationOffset, 6, this.theme.baseColor);
          }
        } else {
          this.drawTickAtRadian(this.outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + this.currentRotationOffset, 2, this.theme.baseColor);
        }
        if (hour % 12 === 0) {
          formattedHour = 12;
        } else {
          formattedHour = hour % 12;
        }
        _results.push(this.drawTextAtRadian(this.outputContext, (hour * -HOUR_RADIAN) + QUARTER_CIRCLE + this.currentRotationOffset, 20, formattedHour, this.theme.iconColor));
      }
      return _results;
    };

    Clock.prototype.getRadianForTime = function(dateTime, relativeTo) {
      var angleForTime, duration, percentOfArc;
      if (relativeTo == null) {
        relativeTo = this.currentTime;
      }
      duration = dateTime - relativeTo;
      percentOfArc = duration / MILLISECONDS_IN_A_DAY;
      angleForTime = percentOfArc * FULL_CIRCLE;
      return angleForTime;
    };

    Clock.prototype.clear = function() {
      return this.context.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);
    };

    Clock.prototype.updateClock = function() {
      this.draw();
    };

    Clock.prototype.drawBase = function() {
      var context;
      context = this.baseCanvas.getContext('2d');
      context.clearRect(0, 0, this.baseCanvas.width, this.baseCanvas.height);
      context.strokeStyle = this.theme.baseColor;
      context.lineWidth = ARC_THICKNESS;
      context.beginPath();
      context.arc(this.centerPoint, this.centerPoint, ARC_RADIUS, 0, FULL_CIRCLE, false);
      context.closePath();
      context.stroke();
      context.strokeStyle = this.theme.iconColor;
      context.lineWidth = 1;
      context.beginPath();
      context.moveTo(this.centerPoint, this.centerPoint - ARC_RADIUS - ARC_THICKNESS - (ICON_SIZE + ICON_PADDING));
      context.lineTo(this.centerPoint, this.centerPoint - ARC_RADIUS - ARC_THICKNESS - ICON_PADDING);
      context.closePath();
      return context.stroke();
    };

    Clock.prototype.draw = function() {
      var diff, ex, ey, sSr, sSs, ssDiff, sx, sy;
      this.context.clearRect(0, 0, this.clockCanvas.width, this.clockCanvas.height);

      /*
      		@context.strokeStyle = @theme.highlightColor
      		@context.lineWidth = GLOW_ARC_THICKNESS
      		@context.beginPath()
      		sSr = @currentRotationOffset+@sunriseRadian
      		sSs = @currentRotationOffset+@sunsetRadian
      		ssDiff = @sunsetRadian - @sunriseRadian
      		ex = @centerPoint + ARC_RADIUS * Math.sin(sSs)
      		ey = @centerPoint + ARC_RADIUS * Math.cos(sSs)
      		@context.arc(@centerPoint, @centerPoint, ARC_RADIUS, CANVAS_ARC_OFFSET_RADIAN - sSr, ssDiff, false);
      		 * this keeps it from closing the arc
      		@context.moveTo(ex, ey);
      		@context.closePath()
      		@context.stroke();
       */
      this.drawSunPathArcs(this.sunrise, this.sunset, ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.dayColor);
      diff = Math.abs(this.sunrise - this.sunset);
      this.drawSunPathArcs(this.sunset, this.sunset.valueOf() + diff, ARC_RADIUS, GLOW_ARC_THICKNESS, this.theme.nightColor);
      sx = null;
      sy = null;
      ex = null;
      ey = null;
      sSr = null;
      sSs = null;
      ssDiff = null;
    };

    Clock.prototype.animate = function() {
      var centerRadius, factor;
      this.animationContext.clearRect(0, 0, this.animationCanvas.width, this.animationCanvas.height);
      factor = easing.easeOutQuad(this.pulseFrame, this.pulseFrame, 0.0, 1.0, PULSE_RATE);
      centerRadius = 164 + (30 * factor);
      if (this.animationContext.setStrokeColor) {
        this.animationContext.setStrokeColor(this.theme.faintColor, easing.easeOutYoYo(this.pulseFrame, this.pulseFrame, 0.0, 1.0, PULSE_RATE));
      } else {
        this.animationContext.strokeStyle = this.theme.faintColor;
      }
      this.animationContext.shadowColor = this.theme.baseColor;
      this.animationContext.shadowBlur = 20;
      this.animationContext.shadowOffsetX = 0;
      this.animationContext.shadowOffsetY = 0;
      this.animationContext.lineWidth = 12 - (12 * factor);
      this.animationContext.beginPath();
      this.animationContext.arc(this.centerPoint, this.centerPoint, centerRadius, 0, FULL_CIRCLE);
      this.animationContext.closePath();
      this.animationContext.stroke();
      return this.animationContext.fill();
    };

    Clock.prototype.drawTickAtRadian = function(context, radian, size, color) {
      var ex, ey, sx, sy;
      context.strokeStyle = color;
      context.lineWidth = 1;
      context.beginPath();
      sx = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING)) * Math.sin(radian);
      sy = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING)) * Math.cos(radian);
      ex = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + ICON_PADDING) * Math.sin(radian);
      ey = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + ICON_PADDING) * Math.cos(radian);
      context.moveTo(sx, sy);
      context.lineTo(ex, ey);
      context.closePath();
      return context.stroke();
    };

    Clock.prototype.drawTextAtRadian = function(context, radian, size, text, color) {
      var angle, sx, sy;
      sx = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING + ICON_PADDING)) * Math.sin(radian);
      sy = this.centerPoint + (ARC_RADIUS + ARC_THICKNESS + (size + ICON_PADDING + ICON_PADDING)) * Math.cos(radian);
      angle = Math.atan2(this.centerPoint - sy, this.centerPoint - sx);
      context.fillStyle = color;
      context.textAlign = 'center';
      return context.fillText(text, sx, sy, 25);
    };

    Clock.prototype.drawTimeSpanArc = function(startDate, endDate, radius, size, color, shadowColor) {
      var endAngle, endDateRadian, ex, ey, startAngle, startDateRadian, sx, sy;
      this.context.strokeStyle = color;
      this.context.lineWidth = size;
      this.context.shadowColor = shadowColor;
      this.context.shadowBlur = size;
      this.context.shadowOffsetX = 0;
      this.context.shadowOffsetY = 0;
      this.context.beginPath();
      startDateRadian = this.getRadianForTime(startDate, this.getMidnight(this.currentTime));
      endDateRadian = this.getRadianForTime(endDate, this.getMidnight(this.currentTime));
      sx = this.centerPoint + Math.sin(QUARTER_CIRCLE - startDateRadian) * radius;
      sy = this.centerPoint + Math.cos(QUARTER_CIRCLE - startDateRadian) * radius;
      ex = this.centerPoint + Math.sin(QUARTER_CIRCLE - endDateRadian) * radius;
      ey = this.centerPoint + Math.cos(QUARTER_CIRCLE - endDateRadian) * radius;
      startAngle = Math.atan2(this.centerPoint - sy, this.centerPoint - sx);
      endAngle = Math.atan2(this.centerPoint - ey, this.centerPoint - ex);
      this.context.arc(this.centerPoint, this.centerPoint, radius, startAngle - HALF_CIRCLE, endAngle - HALF_CIRCLE, false);
      this.context.moveTo(ex, ey);
      this.context.closePath();
      return this.context.stroke();
    };

    Clock.prototype.drawSunPathArcs = function(startDate, endDate, radius, size, arcColor) {
      var baseColor, color, halfSegments, i, interval, opacityInterval, span, _i, _ref, _results;
      span = endDate - startDate;
      interval = Math.floor(span / SEGMENTS);
      baseColor = arcColor.replace(/[rgba]+?\((.+)\)/, '$1').split(',');
      halfSegments = Math.floor(SEGMENTS / 2);
      opacityInterval = baseColor[3] / halfSegments;
      _results = [];
      for (i = _i = 0, _ref = SEGMENTS - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        if (i < halfSegments) {
          color = "rgba(" + baseColor[0] + "," + baseColor[1] + "," + baseColor[2] + "," + (opacityInterval * (i + 1)) + ")";
        } else {
          color = "rgba(" + baseColor[0] + "," + baseColor[1] + "," + baseColor[2] + "," + (opacityInterval * (halfSegments - ((i + 1) - halfSegments))) + ")";
        }
        _results.push(this.drawTimeSpanArc(startDate.valueOf() + (interval * i), startDate.valueOf() + (interval * (i + 1)), radius, size, color, this.theme.clearColor));
      }
      return _results;
    };

    Clock.prototype.onAnimationFrameTick = function(tickTime) {
      this.pulseFrame++;
      if (this.pulseFrame >= PULSE_RATE) {
        this.pulseFrame = 0;
      }
      this.currentFrame++;
      if (this.currentFrame >= REFRESH_RATE) {
        this.setTime(new Date());
      }
      return this.animationId = requestAnimationFrame(this.animationFrameProxy);
    };

    Clock.prototype.onDemoAnimationFrameTick = function(tick) {
      var nextMinute;
      nextMinute = new Date(this.currentTime.valueOf() + (60 * 1000));
      this.setTime(nextMinute);
      return this.animationId = requestAnimationFrame(this.demoAnimationFrameProxy);
    };

    return Clock;

  })();

}).call(this);
