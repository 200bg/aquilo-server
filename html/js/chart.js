(function() {
  var CHART_START_ANGLE, TAU;

  window.ccmm = window.ccmm || {};

  TAU = Math.PI * 2;

  CHART_START_ANGLE = TAU / -2;

  ccmm.Chart = (function() {
    function Chart(element, dataArray, theme, colors, options) {
      this.element = element;
      this.dataArray = dataArray;
      this.theme = theme;
      this.colors = colors;
      this.options = options != null ? options : {};
      this.options = this.options || {};
      this.asRadial = this.options.asRadial || true;
      this.asDiff = this.options.asDiff || false;
      this.animated = this.options.animated || false;
      this.animationOffset = this.options.animationOffset || 0;
      this.valueFormat = this.options.valueFormat || '{0}';
      this.showData = this.options.showData || false;
      this.dataPointColor = this.options.dataPointColor || this.theme.clearColor;
      this.dataPointFunction = this.options.dataPointFunction || function(dataValue) {
        var dataString;
        dataString = dataValue.toString();
        if (dataString.length > 3) {
          dataString = dataString.substr(0, 3);
        }
        return dataString;
      };
      this.animationFrameProxy = this.onAnimationFrameTick.bind(this);
      this.canvas = this.element.querySelector('canvas.chart');
      this.valueElement = this.element.querySelector('.current-value');
      this.ctx = this.canvas.getContext('2d');
      this.width = this.canvas.width;
      this.height = this.canvas.height;
      this.radius = Math.min(this.width, this.height) / 2;
      this.startRadius = Math.ceil(this.radius / 1.75);
      this.maxRadius = this.radius - this.startRadius;
      this.center = {
        x: Math.floor(this.width / 2),
        y: Math.floor(this.height / 2)
      };
      this.ctx.lineWidth = 1.0;
      this.highColor = this.theme.highColor;
      this.mediumColor = this.theme.mediumColor;
      this.lowColor = this.theme.lowColor;
      this.colors = this.colors || [this.theme.iconColor, this.theme.highlightColor];
      this.currentSpoke = 0;
      this.currentFrame = 0;
      this.offsetRan = this.animationOffset === 0 || false;
      this.totalFrames = 45;
      this.totalBarFrames = this.totalFrames + Math.floor(this.totalFrames * 0.33);
      this.totalBarFramesArray = [];
      if (this.asDiff) {
        this.diffDataArray = [];
      }
      if (!this.dataArray) {
        return;
      } else {
        setData(this.dataArray);
      }
    }

    Chart.prototype.animate = function() {
      if (this.animated && this.dataArray !== null) {
        this.ctx.clearRect(0, 0, this.width, this.height);
        this.offsetRan = this.animationOffset === 0 || false;
        this.currentFrame = 0;
        return this.setData(this.dataArray);
      }
    };

    Chart.prototype.setCurrentValue = function(value) {
      this.valueElement.innerHTML = this.valueFormat.replace(/\{0\}/g, value);
      if (this.animated) {
        this.valueElement.classList.add('animated-out');
        document.body.clientWidth;
        return this.valueElement.classList.remove('animated-out');
      }
    };

    Chart.prototype.setData = function(dataArray, currentValue) {
      var frameCount, i, _i, _ref;
      this.dataArray = dataArray;
      if (currentValue == null) {
        currentValue = null;
      }
      this.updateData(this.dataArray);
      if (this.animated) {
        for (i = _i = 0, _ref = this.dataArray.length; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          frameCount = easypeasy.sineOut(i / this.dataArray.length);
          this.totalBarFramesArray[i] = frameCount;
        }
        this.onAnimationFrameTick(0);
      } else {
        this.currentFrame = this.totalFrames;
        this.currentSpoke = this.dataArray.length;
        this.totalFrames[this.dataArray.length - 1] = 5;
        if (!this.asRadial) {
          this.setStroke(this.width, this.height);
          this.drawGradient();
        } else {
          this.setRadialStroke(this.radius);
          this.drawRadialGradient();
        }
      }
    };

    Chart.prototype.onAnimationFrameTick = function(tickTime) {
      if (!this.offsetRan) {
        this.animationId = requestAnimationFrame(this.animationFrameProxy);
        this.currentFrame++;
        if (this.currentFrame > this.animationOffset) {
          this.offsetRan = true;
          this.currentFrame = 0;
        } else {
          return;
        }
      }
      if (!this.asRadial) {
        this.setStroke(this.width, this.height);
        this.drawGradient();
      } else {
        this.setRadialStroke(this.radius);
        this.drawRadialGradient();
      }
      this.currentFrame++;
      if (this.currentFrame <= this.totalFrames) {
        this.currentSpoke = Math.ceil(easypeasy.sineOut(this.currentFrame / this.totalFrames) * this.dataArray.length);
      } else {
        this.currentSpoke = this.dataArray.length;
      }
      if (this.currentFrame < this.totalBarFrames) {
        this.animationId = requestAnimationFrame(this.animationFrameProxy);
      }
    };

    Chart.prototype.draw = function() {
      var interval, _i, _ref, _results;
      this.ctx.clearRect(0, 0, this.width, this.height);
      _results = [];
      for (interval = _i = _ref = this.width; _ref <= 1 ? _i <= 1 : _i >= 1; interval = _ref <= 1 ? ++_i : --_i) {
        if (interval < this.dataArray.length) {
          this.ctx.strokeStyle = this.colorForLevel(this.dataArray[interval] / this.max, this.colors);
          this.ctx.beginPath();
          this.ctx.moveTo(interval, this.height);
          this.ctx.lineTo(interval, Math.floor(this.height * (this.dataArray[interval] / this.max)));
          this.ctx.closePath();
          _results.push(this.ctx.stroke());
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Chart.prototype.setStroke = function(width, height) {
      var color, i, step, _i, _len, _ref;
      this.gradient = this.ctx.createLinearGradient(width / 2, height, width / 2, 0);
      step = 1.0 / this.colors.length;
      _ref = this.colors;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        color = _ref[i];
        this.gradient.addColorStop(step * i, color);
      }
      return this.ctx.strokeStyle = this.gradient;
    };

    Chart.prototype.setRadialStroke = function(radius) {
      var color, i, step, _i, _len, _ref;
      this.gradient = this.ctx.createRadialGradient(this.center.x, this.center.y, this.startRadius, this.center.x, this.center.y, this.radius);
      step = 1.0 / this.colors.length;
      _ref = this.colors;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        color = _ref[i];
        this.gradient.addColorStop(step * i, color);
      }
      this.ctx.fillStyle = this.gradient;
      return this.ctx.strokeStyle = this.gradient;
    };

    Chart.prototype.drawSpoke = function(startAngle, angleInterval, radius) {
      var endX, endY, startX, startY;
      startX = this.center.x + (this.startRadius * Math.sin(startAngle));
      startY = this.center.y + (this.startRadius * Math.cos(startAngle));
      endX = startX + (radius * Math.sin(startAngle));
      endY = startY + (radius * Math.cos(startAngle));
      this.ctx.lineTo(startX, startY);
      this.ctx.lineTo(endX, endY);
      startX = this.center.x + (this.startRadius * Math.sin(startAngle + angleInterval));
      startY = this.center.y + (this.startRadius * Math.cos(startAngle + angleInterval));
      endX = startX + (radius * Math.sin(startAngle + angleInterval));
      endY = startY + (radius * Math.cos(startAngle + angleInterval));
      this.ctx.lineTo(endX, endY);
      return this.ctx.lineTo(startX, startY);
    };

    Chart.prototype.drawRadialGradient = function() {
      var angleInterval, colorInterval, currentAngle, dataArray, dataString, dataValue, i, p, previousAngle, previousRadius, radius, startAngle, textStartAngle, value, _i, _len;
      colorInterval = 1.0 / this.colors.length;
      this.ctx.clearRect(0, 0, this.width, this.height);
      startAngle = CHART_START_ANGLE;
      angleInterval = TAU / this.dataArray.length;
      this.ctx.moveTo(this.center.x, this.center.y);
      previousRadius = 0;
      previousAngle = startAngle;
      this.ctx.globalCompositeOperation = 'source-over';
      this.setRadialStroke();
      this.ctx.beginPath();
      if (this.asDiff) {
        dataArray = this.diffDataArray;
      } else {
        dataArray = this.dataArray;
      }
      for (i = _i = 0, _len = dataArray.length; _i < _len; i = ++_i) {
        dataValue = dataArray[i];
        if (i > this.currentSpoke) {
          break;
        }
        p = this.currentFrame / this.totalBarFrames;
        p = p * this.totalBarFramesArray[i] + (this.currentFrame / this.totalBarFrames);
        if (p > 1.0) {
          p = 1.0;
        }
        value = dataValue / this.max;
        radius = Math.floor(this.maxRadius * value);
        radius = easypeasy.backOutCustom(p, 1.25) * radius;
        this.drawSpoke(previousAngle, angleInterval, radius);
        currentAngle = startAngle + (i * angleInterval);
        if (this.showData) {
          dataString = this.dataPointFunction(this.dataArray[i]).toString();
          if (i === 0) {
            textStartAngle = previousAngle - angleInterval;
          } else {
            textStartAngle = previousAngle;
          }
          this.drawTextAtRadian(textStartAngle, angleInterval, 10, dataString, this.dataPointColor, this.startRadius * -1.02);
        }
        previousRadius = radius;
        previousAngle = currentAngle;
      }
      if (this.currentSpoke >= this.dataArray.length) {
        this.drawSpoke(startAngle - angleInterval, angleInterval, radius);
      }
      this.ctx.moveTo(this.center.x + this.startRadius, this.center.y);
      this.ctx.closePath();
      this.ctx.globalCompositeOperation = 'destination-over';
      this.ctx.fill();
      this.ctx.beginPath();
      this.ctx.globalCompositeOperation = 'destination-out';
      this.ctx.fillStyle = 'white';
      this.ctx.arc(this.center.x, this.center.y, this.startRadius, 0, TAU);
      this.ctx.closePath();
      this.ctx.fill();
      return this.ctx.globalCompositeOperation = 'source-over';
    };

    Chart.prototype.drawTextAtRadian = function(startRadian, angleInterval, size, text, color, radius) {
      var centerOffset, charCount, oldFillStyle, radian, rotation, sx, sy;
      if (radius == null) {
        radius = this.startRadius * -1.02;
      }
      oldFillStyle = this.ctx.fillStyle;
      this.ctx.fillStyle = color;
      this.ctx.textAlign = 'center';
      charCount = text.length;
      centerOffset = 0;
      if (charCount > 1) {
        centerOffset = Math.floor(size / 2) * (Math.floor(charCount / 2));
      } else {
        centerOffset = Math.floor(size / 4);
      }
      radian = startRadian + (angleInterval / 2);
      radian -= centerOffset / radius;
      sx = centerOffset;
      sy = radius;
      rotation = (Math.PI * 2) - (CHART_START_ANGLE + radian);
      this.ctx.translate(this.center.x, this.center.y);
      this.ctx.rotate(rotation);
      this.ctx.moveTo(sx, sy);
      this.ctx.fillText(text, sx, sy, size);
      this.ctx.moveTo(0, 0);
      this.ctx.rotate(-rotation);
      this.ctx.translate(-this.center.x, -this.center.y);
      return this.ctx.fillStyle = oldFillStyle;
    };

    Chart.prototype.drawGradient = function() {
      var colorInterval, height, interval, value, x, _i, _ref, _results;
      colorInterval = 1.0 / this.colors.length;
      this.ctx.clearRect(0, 0, this.width, this.height);
      _results = [];
      for (interval = _i = 0, _ref = this.width; 0 <= _ref ? _i <= _ref : _i >= _ref; interval = 0 <= _ref ? ++_i : --_i) {
        if (interval < this.dataArray.length) {
          x = interval;
          value = this.dataArray[interval] / this.max;
          height = Math.floor(this.height * value);
          this.ctx.beginPath();
          this.ctx.moveTo(x, this.height);
          this.ctx.lineTo(x, height);
          this.ctx.closePath();
          _results.push(this.ctx.stroke());
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Chart.prototype.updateData = function() {
      var data, diffMin, i, _i, _j, _len, _len1, _ref, _ref1;
      this.max = 0;
      this.min = 4294967295;
      _ref = this.dataArray;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        data = _ref[_i];
        this.max = Math.max(data, this.max);
        this.min = Math.min(data, this.min);
      }
      if (this.asDiff) {
        diffMin = this.min * 0.93;
        _ref1 = this.dataArray;
        for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
          data = _ref1[i];
          this.diffDataArray[i] = data - diffMin;
        }
        return this.max = this.max - diffMin;
      }
    };

    Chart.prototype.colorForLevel = function(percentage, colors) {
      var a, b, bDiff, color, g, gDiff, i, interval, max, min, nextColor, r, rDiff, relativePercentage, _i, _len;
      interval = 1.0 / colors.length;
      for (i = _i = 0, _len = colors.length; _i < _len; i = ++_i) {
        color = colors[i];
        min = interval * i;
        max = min + interval;
        if (i + 1 < colors.length) {
          nextColor = colors[i + 1];
        } else {
          nextColor = color;
        }
        if (percentage > min && percentage <= max) {
          relativePercentage = (percentage - min) / interval;
          rDiff = parseInt(parseInt(nextColor[0] - color[0]));
          gDiff = parseInt(parseInt(nextColor[1] - color[1]));
          bDiff = parseInt(parseInt(nextColor[2] - color[2]));
          r = parseInt(color[0]) + (rDiff * relativePercentage);
          g = parseInt(color[1]) + (gDiff * relativePercentage);
          b = parseInt(color[2]) + (bDiff * relativePercentage);
          a = Number(color[3]) + (Number(color[3]) - Number(nextColor[3])) * relativePercentage;
          return "rgba(" + (parseInt(r)) + "," + (parseInt(g)) + "," + (parseInt(b)) + "," + a + ")";
        }
      }
    };

    return Chart;

  })();

}).call(this);
