(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  ccmm.ThermostatChart = (function(_super) {
    __extends(ThermostatChart, _super);

    function ThermostatChart(element, dataArray, theme, colors, options) {
      this.element = element;
      this.dataArray = dataArray;
      this.theme = theme;
      this.colors = colors;
      this.options = options != null ? options : {};
      ThermostatChart.__super__.constructor.apply(this, arguments);
      this.targetValueElement = this.element.querySelector('.target-value');
      this.runningTimeElement = this.element.querySelector('.running-time');
    }

    ThermostatChart.prototype.setCurrentValue = function(temp, targetTemp, runningTime) {
      if (this.targetValueElement) {
        this.targetValueElement.innerHTML = targetTemp + '&deg;';
      }
      if (this.runningTimeElement) {
        this.runningTimeElement.innerHTML = runningTime;
      }
      if (this.animated) {
        this.targetValueElement.classList.add('animated-out');
        this.runningTimeElement.classList.add('animated-out');
        document.body.clientWidth;
        this.targetValueElement.classList.remove('animated-out');
        this.runningTimeElement.classList.remove('animated-out');
      }
      return ThermostatChart.__super__.setCurrentValue.call(this, temp);
    };

    ThermostatChart.prototype.animate = function() {
      if (this.animated) {
        this.targetValueElement.classList.add('animated-out');
        this.runningTimeElement.classList.add('animated-out');
        document.body.clientWidth;
        this.targetValueElement.classList.remove('animated-out');
        this.runningTimeElement.classList.remove('animated-out');
      }
      return ThermostatChart.__super__.animate.apply(this, arguments);
    };

    return ThermostatChart;

  })(ccmm.Chart);

  ccmm.WeatherChart = (function(_super) {
    __extends(WeatherChart, _super);

    function WeatherChart(element, dataArray, theme, colors, options) {
      this.element = element;
      this.dataArray = dataArray;
      this.theme = theme;
      this.colors = colors;
      this.options = options != null ? options : {};
      WeatherChart.__super__.constructor.apply(this, arguments);
      this.humidityElement = this.element.querySelector('.humidity');
      this.conditionsElement = this.element.querySelector('.conditions');
    }

    WeatherChart.prototype.setCurrentValue = function(temp, humidity, conditions) {
      if (this.humidityElement) {
        this.humidityElement.innerHTML = humidity + '% humidity';
      }
      if (this.conditionsElement) {
        this.conditionsElement.innerHTML = conditions;
      }
      if (this.animated) {
        this.humidityElement.classList.add('animated-out');
        this.conditionsElement.classList.add('animated-out');
        document.body.clientWidth;
        this.humidityElement.classList.remove('animated-out');
        this.conditionsElement.classList.remove('animated-out');
      }
      return WeatherChart.__super__.setCurrentValue.call(this, temp);
    };

    WeatherChart.prototype.animate = function() {
      if (this.animated) {
        this.humidityElement.classList.add('animated-out');
        this.conditionsElement.classList.add('animated-out');
        document.body.clientWidth;
        this.humidityElement.classList.remove('animated-out');
        this.conditionsElement.classList.remove('animated-out');
      }
      return WeatherChart.__super__.animate.apply(this, arguments);
    };

    return WeatherChart;

  })(ccmm.Chart);

  ccmm.PollenChart = (function(_super) {
    __extends(PollenChart, _super);

    function PollenChart(element, dataArray, theme, colors, options) {
      this.element = element;
      this.dataArray = dataArray;
      this.theme = theme;
      this.colors = colors;
      this.options = options != null ? options : {};
      PollenChart.__super__.constructor.apply(this, arguments);
      this.pollenTypeElement = this.element.querySelector('.pollen-type');
    }

    PollenChart.prototype.setCurrentValue = function(count, type) {
      if (this.pollenTypeElement) {
        this.pollenTypeElement.innerHTML = type;
      }
      if (this.animated) {
        this.pollenTypeElement.classList.add('animated-out');
        document.body.clientWidth;
        this.pollenTypeElement.classList.remove('animated-out');
      }
      return PollenChart.__super__.setCurrentValue.call(this, count);
    };

    PollenChart.prototype.animate = function() {
      if (this.animated) {
        this.pollenTypeElement.classList.add('animated-out');
        document.body.clientWidth;
        this.pollenTypeElement.classList.remove('animated-out');
      }
      return PollenChart.__super__.animate.apply(this, arguments);
    };

    return PollenChart;

  })(ccmm.Chart);

  ccmm.EnvironmentalControls = (function(_super) {
    var POLLEN_API, WEATHER_API, WEATHER_TEST;

    __extends(EnvironmentalControls, _super);

    WEATHER_API = 'http://api.openweathermap.org/data/2.5/weather?q={0}';

    POLLEN_API = 'http://www.claritin.com/weatherpollenservice/weatherpollenservice.svc/getforecast/{0}';

    WEATHER_TEST = {
      "coord": {
        "lon": -95.37,
        "lat": 29.76
      },
      "sys": {
        "message": 0.051,
        "country": "United States of America",
        "sunrise": 1399289694,
        "sunset": 1399338080
      },
      "weather": [
        {
          "id": 800,
          "main": "Clear",
          "description": "Sky is Clear",
          "icon": "01n"
        }
      ],
      "base": "cmc stations",
      "main": {
        "temp": 290.91,
        "pressure": 1014,
        "temp_min": 288.15,
        "temp_max": 293.71,
        "humidity": 91
      },
      "wind": {
        "speed": 3.22,
        "deg": 180.503
      },
      "clouds": {
        "all": 0
      },
      "dt": 1399269629,
      "id": 4699066,
      "name": "Houston",
      "cod": 200
    };

    function EnvironmentalControls(element, location, theme) {
      var ambientOptions, pollenOptions, weatherOptions;
      this.element = element;
      this.location = location;
      this.theme = theme;
      this.weatherUrl = WEATHER_API.replace('{0}', this.location.name);
      this.weatherData = WEATHER_TEST;
      ambientOptions = {
        asRadial: true,
        asDiff: true,
        animated: true,
        animationOffset: 0,
        valueFormat: '{0}&deg;',
        showData: true,
        dataPointColor: this.theme.background,
        dataPointFunction: Math.floor
      };
      this.ambientChart = new ccmm.ThermostatChart(document.getElementById('ambient-chart'), null, this.theme, null, ambientOptions);
      weatherOptions = {
        asRadial: true,
        asDiff: true,
        animated: true,
        animationOffset: 15,
        valueFormat: '{0}&deg;',
        showData: true,
        dataPointColor: this.theme.background,
        dataPointFunction: function(d) {
          return Math.floor(d) + '°';
        }
      };
      this.weatherChart = new ccmm.WeatherChart(document.getElementById('weather-chart'), null, this.theme, [this.theme.alertColor, this.theme.highColor], weatherOptions);
      pollenOptions = {
        asRadial: true,
        animated: true,
        showData: true,
        dataPointColor: this.theme.background,
        animationOffset: 30
      };
      this.pollenChart = new ccmm.PollenChart(document.getElementById('pollen-chart'), null, this.theme, [this.theme.dateDayColor, this.theme.mediumColor], pollenOptions);
      return;
    }

    EnvironmentalControls.prototype.activate = function() {
      EnvironmentalControls.__super__.activate.apply(this, arguments);
      this.ambientChart.animate();
      this.weatherChart.animate();
      return this.pollenChart.animate();
    };

    EnvironmentalControls.prototype.getWeather = function() {
      return ajax(this.weatherUrl, this.onWeatherReceivedProxy, 'GET', null, 'application/json');
    };

    EnvironmentalControls.prototype.displayWeather = function() {
      this.weatherData;
    };

    EnvironmentalControls.prototype.onWeatherReceived = function(error, text, xhr) {
      return this.weatherData = JSON.parse(text);
    };

    return EnvironmentalControls;

  })(ccmm.Section);

}).call(this);
