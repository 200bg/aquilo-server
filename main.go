// +build aquilo-server

package main

import (
	"bitbucket.org/200bg/aquilo-server/aquilo"
	"bitbucket.org/200bg/aquilo-server/aquilo/config"
	"bitbucket.org/200bg/aquilo-server/aquilo/csrf"
	"bitbucket.org/200bg/aquilo-server/aquilo/data"
	"bufio"
	"database/sql"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/howeyc/gopass"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"math/rand"
	"syscall"
)

var signalChannel = make(chan os.Signal, 1)
var cmd = new(exec.Cmd)

func exitGracefully() {
	for sig := range signalChannel {
		if sig == syscall.SIGQUIT {
			log.Println("Aquilo shutting down.")
			println("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓")
			os.Exit(0)
		}
		if sig == syscall.SIGUSR2 {
			log.Println("Aquilo restarting.")
			println("↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕↕")
			os.Exit(0)
		}
		if sig == syscall.SIGINT {
			os.Exit(0)
		}
	}
	
	if cmd != nil {
		log.Println("Killing Grunt process.")
		cmd.Process.Kill()	
	}
}

func gruntWatch() {
	cmd = exec.Command("grunt", "watch")
    cmd.Stdout = os.Stdout
    cmd.Stderr = os.Stderr
    cmd.Run()
}

func handleUserEdits(createUser bool, setPassword bool) {
	// if create user, then do this thing, then bail
	var username string
	var name string
	if (createUser || setPassword) {
		
		fmt.Printf("Email: ")
		_, err := fmt.Scanln(&username)
		
		if err == nil {
			fmt.Printf("\nLooking up username \"%s\"...", username)
			conn, err := config.GlobalConfig.ConnectToDb()
			defer conn.Close()
			if err != nil {
				fmt.Println("Error connecting to database.")
				os.Exit(1)
			}
			
			if data.AquiloUserExists(conn, username) {
				fmt.Println("Exists.")
			} else {
				fmt.Println("Not found. Good to go.")
				// if we're creating the user
				if !setPassword {
					setPassword = true
					
					fmt.Printf("Full Name: ")
					scanner := bufio.NewScanner(os.Stdin)
					for scanner.Scan() {
						name = scanner.Text()
						break
					}
					if err = scanner.Err(); err != nil {
						fmt.Fprintln(os.Stderr, "reading standard input:", err)
					}
				}
			}
			conn.Close()
		}
	}
	
	if (setPassword) {
		var pass string
		
		fmt.Printf("Set Password: ")
		pass = string(gopass.GetPasswdMasked())
		
		if pass != "" {
			conn, err := config.GlobalConfig.ConnectToDb()
			defer conn.Close()
			if err != nil {
				fmt.Println("Error connecting to database.")
				os.Exit(1)
			}
			
			randomInt := rand.Int63n(2048)
			
			if !createUser {
				user, err := data.AquiloUserChangePassword(conn, username, pass, randomInt)
				if err == nil {
					fmt.Printf("Password changed for user %s.\n", user.Email)
				} else {
					fmt.Printf("Error changing password. %s", err)	
				}
			} else {
				salt := data.GenerateSalt()
				user := data.AquiloUser {
					Email: username,
					Name: name,
					UserToken: data.GenerateUserToken(conn),
					HashedPassword: sql.NullString {String:data.HashPassword(pass, salt, randomInt)},
					Salt: salt,
					HashCount: randomInt,
					PermissionsLevel: 0,
					IsBanned: false,
				}
				_, err = user.Save(conn)
				if err == nil {
					fmt.Printf("Created user %s.\n", user.Email)
				}
			}
			conn.Close()
			
		}
		os.Exit(0)
	}	
}

func main() {
	// read args
	var configPath string
	var grunt bool
	var createUser bool
	var setPassword bool
	flag.StringVar(&configPath, "c", "/etc/aquilo/aquilo.conf", "Required argument.")
	flag.StringVar(&configPath, "config", "/etc/aquilo/aquilo.conf", "Required argument.")
	flag.BoolVar(&grunt, "g", false, "Development only. Starts Gruntfile watcher.")
	flag.BoolVar(&createUser, "createuser", false, "Initiate a wizard to create user.")
	flag.BoolVar(&setPassword, "setpassword", false, "Initiate a wizard to set a user's password.")
	flag.Parse()

	if len(os.Args) == 1 {
		fmt.Println("Required argument: -config /path/to/aquilo.conf")
		os.Exit(1)
	}

	println("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑")
	log.Println("Loading configuration file", configPath)

	// NewConfig will load into GlobalConfig
	// WHICH IS USED EVERYWHERE
	// don't run this function blindly
	config.NewConfig(configPath)
	
	if createUser || setPassword {
		// graceful shutdowns
		signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGUSR2)
		go exitGracefully()
		
		handleUserEdits(createUser, setPassword)
		
		// handleUserEdits will exit rather than start the server.
	}

	log.Printf("Aquilo Web Server %s started on port %d.", aquilo.VERSION, config.GlobalConfig.Server.Port)
	// create the master that will spawn the workers
	// master := aquilo.NewMaster()

	// graceful shutdowns
	signal.Notify(signalChannel, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGUSR2)
	go exitGracefully()
	
	// Grunt process
	if (grunt) {
		go gruntWatch()
	}
	
	
	// load and store the templates in RAM
	aquilo.LoadTemplates()

	r := mux.NewRouter()
	r.HandleFunc("/", aquilo.HomeHandler)
	// login
	r.HandleFunc("/login/", aquilo.LoginHandler)
	// device registration
	r.HandleFunc("/device/register/", aquilo.RegisterDeviceHandler)
	// vitals from registration
	r.HandleFunc("/device/vitals/", aquilo.VitalsHandler)
	
	exemptions := []string {
		"/device/register/",
		"/device/vitals/",
	}
	csrfMiddler := csrf.CSRFMiddleware(r, exemptions)
	
	// csrf
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./html"))))
	http.Handle("/", csrfMiddler)
	
	serverString := fmt.Sprintf("%s:%d", config.GlobalConfig.Server.Hostname, config.GlobalConfig.Server.Port)
	http.ListenAndServe(serverString, nil)
}
