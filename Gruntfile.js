module.exports = function (grunt) {
    grunt.initConfig({
        less: {
          dist: {
            files: {
              // 'html/css/*.css': 'html/src/css/*.less'
              'html/css/ccmm.css': 'html/src/css/ccmm.less'
            }
          }
        },
        coffee: {
          dist: {
            files: {
              'html/js/ccmm.themes.js': 'html/src/js/ccmm.themes.coffee',
              'html/js/ccmm.js': 'html/src/js/ccmm.coffee',
              'html/js/chart.js': 'html/src/js/chart.coffee',
              'html/js/easypeasy.js': 'html/src/js/easypeasy.coffee',
              'html/js/environmental.js': 'html/src/js/environmental.coffee',
            }
          }
        },
        jade: {
          compile: {
            files: {
              'html/index.html': 'html/src/index.jade',
              'html/chart.html': 'html/src/chart.jade',
              'html/login.html': 'html/src/login.jade',
            }
          }
        },
        watch: {
          scripts: {
            files: ['html/src/**/*.js', 'html/src/css/*.less', 'html/src/*.jade'],
            tasks: ['coffee', 'less', 'jade'],
            options: {
              spawn: false,
            },
          },
        },
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jade');

    grunt.registerTask('default', ['coffee', 'less', 'jade']);
    grunt.registerTask('server', ['coffee', 'watch']);
    // grunt.registerTask('default', ['6to5', 'uglify']);

};